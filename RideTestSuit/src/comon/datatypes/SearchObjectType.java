/**
Dulan
*/
package comon.datatypes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Properties;
import java.util.Queue;
import java.util.Random;
import java.util.UUID;

import org.apache.log4j.Logger;

import rezg.ride.car.server.datatypes.Vehicle;
import ride.common.datatypes.Address;
import ride.common.datatypes.Contact;
import ride.common.datatypes.CreditCard;
import ride.common.datatypes.DailyRate;
import ride.common.datatypes.Hotel;
import ride.common.datatypes.HotelList;
import ride.common.datatypes.Rate;
import ride.common.datatypes.Room;
import ride.common.datatypes.RoomOccupant;
import ride.common.datatypes.Tax;
import ride.common.hotelavailability.AvailabilityResponse;
import ride.common.prereservationcheck.PreReservationResponse;
import ride.common.reservation.ReservationResponse;
import rideTest.utills.PG_Properties;

public class SearchObjectType {

	
	private TestMethod          testMethod                               = null;
	private TestType            testType                                 = null;
	private	String              name                                     = null;
	private	String              ConnectionSerever                        = null;
	private	String              id			                             = null;
	private String              vendorid 	                             = null;
	private String              schema    	                             = null;
	private Object[][] 	        markups 	                             = null;
	private double[] 	        markup 		                             = new double[16];
	private String[][] 	        vendors 	                             = null;
	private String              PortalName                               = null;
	private String              State  			                         = "-";
	private String              countryCode  	                         = "54"; 
	private String              cityCode    	                         = "-";
	private String              suburbcode    	                         = null;
    private int               numberOfRooms 	                            = 1;
	private int               TotnoOfAdults 	                            = 0;
	private int            TotnoOfChildrens 	                        = 0;
	private String portal                                   = null;
	private RoomOccupant[] roomOccupants                    = null;
    private String cacheKey                                 = null;
	private	AvailabilityResponse response                   = null;
	private	HotelList hotels                                = null;
	private	Hotel Test2Hotel                                = null;  
	//rezg.dto.ride.common.datatypes.Hotel Test2Hotelrezg = new rezg.dto.ride.common.datatypes.Hotel(); 
	private	ArrayList errors              = null;
    private	ReservationResponse resp = null;
	private	String[] iteno                                    = null;
	private	String  resNum []                                 = null; 
    private	Contact[] contact                                 = null;
	private	Address[] address                                 = null;
	private	CreditCard card                                   = null;
	private	int count                                         = 0;
	private	String SelectedRoomCode							  = null;
	private	Properties	vendorProps 						  = null;
    private	Vehicle resVehicleObj                             = null;
	private String HotelCode                                  = "";
	private Date CheckInDate;
	private Date CheckOutDate;
	private String NOR                                        = "";
	private int    NON                                        = 0 ;
	private String Room1AD                                    = "";
	private String Room1CH        = "";
	private String Room2AD        = "";
	private String Room2CH        = "";
	private String Room3AD        = "";
	private String Room3CH        = "";
	private String Room4AD        = "";
	private String Room4CH        = "";
	private String RoomOneChildAges  = "";
	private String RoomTwoChildAges  = "";
	private String RoomThreeChildAges= "";
	private String RoomFourChildAges = "";
	private String UniqueID          ="";
	private String CityName               ="";
	private ArrayList<String> vendorList  = null;
	
	
	
	
	
	
	
	public ArrayList<String> getVendorList() {
		return vendorList;
	}


	public void setVendorList(ArrayList<String> vendorList) {
		this.vendorList = vendorList;
	}


	public String getPortalName() {
		return PortalName;
	}


	public void setPortalName(String portalName) {
		PortalName = portalName;
		schema     = portalName;
		portal     = portalName+"_hotels";
	}


	public TestType getTestType() {
		return testType;
	}


	public void setTestType(TestType testType) {
		this.testType = testType;
	}


	public String getConnectionSerever() {
		
		return ConnectionSerever;
	}


	public void setConnectionSerever(String connectionSerever) {
		
		if(connectionSerever.equalsIgnoreCase("MC6STG"))
		ConnectionSerever = "ride-test.secure-reservation.com";
		else if(connectionSerever.equalsIgnoreCase("MCLUK13"))
	    ConnectionSerever = "ride-backup.secure-reservation.com:81";
		else if(connectionSerever.equalsIgnoreCase("126"))
		ConnectionSerever = "192.168.1.126:9090";
		else if(connectionSerever.equalsIgnoreCase("Localhost"))
		ConnectionSerever = "localhost";
		else if(connectionSerever.equalsIgnoreCase("MC6LIVE"))
		ConnectionSerever = "ride.secure-reservation.com";
		else if(connectionSerever.equalsIgnoreCase("IP"))
	    ConnectionSerever = PG_Properties.getProperty("Ride.Server");
		else 
		ConnectionSerever = connectionSerever;
			
	}


	public String getCityName() {
		return CityName;
	}


	public void setCityName(String cityName) {
		CityName = cityName;
	}


	private Queue<TestType> order    = new ArrayDeque<TestType>();



	
   public TestMethod getTestMethod() {
		return testMethod;
	}


	public void setTestMethod(TestMethod testMethod) {
		this.testMethod = testMethod;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getVendorid() {
		return vendorid;
	}


	public void setVendorid(String vendorid) {
		this.vendorid = vendorid;
	}


	public String getSchema() {
		return schema;
	}


	public void setSchema(String schema) {
		this.schema = schema;
	}


	public Object[][] getMarkups() {
		return markups;
	}


	public void setMarkups(Object[][] markups) {
		this.markups = markups;
	}


	public double[] getMarkup() {
		return markup;
	}


	public void setMarkup(double[] markup) {
		this.markup = markup;
	}


	public String[][] getVendors() {
		return vendors;
	}


	public void setVendors(String[][] vendors) {
		this.vendors = vendors;
	}


	public String getState() {
		return State;
	}


	public void setState(String state) {
		State = state;
	}


	public String getCountryCode() {
		return countryCode;
	}


	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}


	public String getCityCode() {
		return cityCode;
	}


	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}


	public String getSuburbcode() {
		return suburbcode;
	}


	public void setSuburbcode(String suburbcode) {
		this.suburbcode = suburbcode;
	}


	public int getNumberOfRooms() {
		return numberOfRooms;
	}


	public void setNumberOfRooms(int numberOfRooms) {
		this.numberOfRooms = numberOfRooms;
	}


	public int getTotnoOfAdults() {
		return TotnoOfAdults;
	}


	public void setTotnoOfAdults(int totnoOfAdults) {
		TotnoOfAdults = totnoOfAdults;
	}


	public int getTotnoOfChildrens() {
		return TotnoOfChildrens;
	}


	public void setTotnoOfChildrens(int totnoOfChildrens) {
		TotnoOfChildrens = totnoOfChildrens;
	}


	public String getPortal() {
		return portal;
	}


	public void setPortal(String portal) {
		this.portal = portal;
		schema      = portal;
	}


	public RoomOccupant[] getRoomOccupants() {
		return roomOccupants;
	}


	public void setRoomOccupants(RoomOccupant[] roomOccupants) {
		this.roomOccupants = roomOccupants;
	}


	public String getCacheKey() {
		return cacheKey;
	}


	public void setCacheKey(String cacheKey) {
		this.cacheKey = cacheKey;
	}


	public AvailabilityResponse getResponse() {
		return response;
	}


	public void setResponse(AvailabilityResponse response) {
		this.response = response;
	}


	public HotelList getHotels() {
		return hotels;
	}


	public void setHotels(HotelList hotels) {
		this.hotels = hotels;
	}


	public Hotel getTest2Hotel() {
		return Test2Hotel;
	}


	public void setTest2Hotel(Hotel test2Hotel) {
		Test2Hotel = test2Hotel;
	}


	public ArrayList getErrors() {
		return errors;
	}


	public void setErrors(ArrayList errors) {
		this.errors = errors;
	}


	public ReservationResponse getResp() {
		return resp;
	}


	public void setResp(ReservationResponse resp) {
		this.resp = resp;
	}


	public String[] getIteno() {
		return iteno;
	}


	public void setIteno(String[] iteno) {
		this.iteno = iteno;
	}


	public String[] getResNum() {
		return resNum;
	}


	public void setResNum(String[] resNum) {
		this.resNum = resNum;
	}


	public Contact[] getContact() {
		return contact;
	}


	public void setContact(Contact[] contact) {
		this.contact = contact;
	}


	public Address[] getAddress() {
		return address;
	}


	public void setAddress(Address[] address) {
		this.address = address;
	}


	public CreditCard getCard() {
		return card;
	}


	public void setCard(CreditCard card) {
		this.card = card;
	}


	public int getCount() {
		return count;
	}


	public void setCount(int count) {
		this.count = count;
	}


	public String getSelectedRoomCode() {
		return SelectedRoomCode;
	}


	public void setSelectedRoomCode(String selectedRoomCode) {
		SelectedRoomCode = selectedRoomCode;
	}


	public Properties getVendorProps() {
		return vendorProps;
	}


	public void setVendorProps(Properties vendorProps) {
		this.vendorProps = vendorProps;
	}


	public Vehicle getResVehicleObj() {
		return resVehicleObj;
	}


	public void setResVehicleObj(Vehicle resVehicleObj) {
		this.resVehicleObj = resVehicleObj;
	}


	public String getRoomOneChildAges() {
		return RoomOneChildAges;
	}


	public void setRoomOneChildAges(String roomOneChildAges) {
		RoomOneChildAges = roomOneChildAges;
	}


	public String getRoomTwoChildAges() {
		return RoomTwoChildAges;
	}


	public void setRoomTwoChildAges(String roomTwoChildAges) {
		RoomTwoChildAges = roomTwoChildAges;
	}


	public String getRoomThreeChildAges() {
		return RoomThreeChildAges;
	}


	public void setRoomThreeChildAges(String roomThreeChildAges) {
		RoomThreeChildAges = roomThreeChildAges;
	}


	public String getRoomFourChildAges() {
		return RoomFourChildAges;
	}


	public void setRoomFourChildAges(String roomFourChildAges) {
		RoomFourChildAges = roomFourChildAges;
	}


	public Queue<TestType> getOrder() {
		return order;
	}


	public void setOrder(Queue<TestType> order) {
		this.order = order;
	}


	public void setCheckInDate(Date checkInDate) {
		CheckInDate = checkInDate;
	}


	public void setCheckOutDate(Date checkOutDate) {
		CheckOutDate = checkOutDate;
	}


	public void setNON(int nON) {
		NON = nON;
	}


public String getRoom4AD() {
	return Room4AD;
}


public void setRoom4AD(String room4ad) {
	Room4AD = room4ad;
}


public String getRoom4CH() {
	return Room4CH;
}


public void setRoom4CH(String room4ch) {
	Room4CH = room4ch;
}


public TestType dequeue() {
	
	return order.poll();

}


public void setOrder(TestType type) {
	if(type == TestType.CANCELLATION)
	{
		order.add(TestType.AVAILABILITY);
		order.add(TestType.ROOMAVAILABILITY);
		order.add(TestType.PRERASERAVATION);
		order.add(TestType.RESERAVATION);
		order.add(TestType.CANCELLATION);
	}else
	if(type == TestType.RESERAVATION)
	{
		order.add(TestType.AVAILABILITY);
		order.add(TestType.ROOMAVAILABILITY);
		order.add(TestType.PRERASERAVATION);
		order.add(TestType.RESERAVATION);
	
	}else
	if(type == TestType.PRERASERAVATION)
	{
		order.add(TestType.AVAILABILITY);
		order.add(TestType.ROOMAVAILABILITY);
		order.add(TestType.PRERASERAVATION);

	}else
	if(type == TestType.ROOMAVAILABILITY)
	{
		order.add(TestType.AVAILABILITY);
		order.add(TestType.ROOMAVAILABILITY);

	}else
	if(type == TestType.AVAILABILITY)
	{
		order.add(TestType.AVAILABILITY);
    }
}


public SearchObjectType ()
{
   this.setUniqueID(UUID.randomUUID().toString());
}


public  String getHotelCode() {
	return HotelCode;
}
public void setHotelCode(String hotelCode) {
	HotelCode = hotelCode;
}
public Date getCheckInDate() {
	return CheckInDate;
}
public void setCheckInDate(String checkInDate) throws ParseException {
	if(this.testMethod == TestMethod.Perfomance)
	{
		Random rand = new Random();
		String SearchDateRange  = PG_Properties.getProperty("SearchDateRange");
		String OmmitedRange     = PG_Properties.getProperty("OmmitedDateRange");
        int  n                  = rand.nextInt(Integer.parseInt(SearchDateRange)) + Integer.parseInt(OmmitedRange);
	    Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, n);
        CheckInDate  = new SimpleDateFormat("MMM-dd-yyyy'T'HH:mm:ss",Locale.ENGLISH).parse(new SimpleDateFormat("MMM-dd-yyyy").format(cal.getTime())+"T09:00:00");
        
		
	}
	else if(this.testMethod == TestMethod.Normal)
	CheckInDate = new SimpleDateFormat("MMM-dd-yyyy'T'HH:mm:ss",Locale.ENGLISH).parse(checkInDate+"T09:00:00");
}
public Date getCheckOutDate() {
	return CheckOutDate;
}
public void setCheckOutDate(String checkOutDate) throws ParseException {
	CheckOutDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss",Locale.ENGLISH).parse(checkOutDate);
}
public String getNOR() {
	return NOR;
}
public void setNOR(String nOR) {
	NOR = nOR;
}
public int getNON() {
	return NON;
}
public void setNON(String nON) throws ParseException {
	NON = Integer.parseInt(nON);
	Calendar cal = Calendar.getInstance();
	cal.setTime(CheckInDate);
	cal.add(Calendar.DATE, NON);
	CheckOutDate = cal.getTime();
}
public String getRoom1AD() {
	return Room1AD;
}
public void setRoom1AD(String room1ad) {
	Room1AD = room1ad;
}
public String getRoom1CH() {
	return Room1CH;
}
public void setRoom1CH(String room1ch) {
	Room1CH = room1ch;
}
public String getRoom2AD() {
	return Room2AD;
}
public void setRoom2AD(String room2ad) {
	Room2AD = room2ad;
}
public String getRoom2CH() {
	return Room2CH;
}
public void setRoom2CH(String room2ch) {
	Room2CH = room2ch;
}
public String getRoom3AD() {
	return Room3AD;
}
public void setRoom3AD(String room3ad) {
	Room3AD = room3ad;
}
public String getRoom3CH() {
	return Room3CH;
}
public void setRoom3CH(String room3ch) {
	Room3CH = room3ch;
}
public String getUniqueID() {
	return UniqueID;
}


public void setUniqueID(String uniqueID) {
	UniqueID = uniqueID;
}


}
