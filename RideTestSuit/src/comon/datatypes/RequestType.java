package comon.datatypes;


@SuppressWarnings("unused")
public class RequestType {
	public static String name ="Dulan";


	public RequestType(String type){
		test = type;
	}
	
	public String getTest() {
		return test;
	}
	
	public	String test 		= null;
		
	public static RequestType hotelSearchCall 			= new RequestType("hotelSearchCall");
	public static RequestType hotelRoomSearchCall 		= new RequestType("hotelRoomSearchCall");
	public static RequestType hotelPreReservationCall 	= new RequestType("hotelPreReservationCall");
	public static RequestType hotelReservationCall 		= new RequestType("hotelReservationCall");
	public static RequestType hotelCancelationCall 		= new RequestType("hotelCancelationCall");
	public static RequestType hotelContent 				= new RequestType("hotelContent");
	
	public static RequestType carSearchCall 				= new RequestType("carSearchCall");
	public static RequestType carContentCall 				= new RequestType("carContentCall");
	public static RequestType carPreReservationCall 		= new RequestType("carPreReservationCall");
	public static RequestType carReservationCall 			= new RequestType("carReservationCall");
	public static RequestType carCancelationCall 			= new RequestType("carCancelationCall");
	
	public static RequestType transferAvailabilityCall 				= new RequestType("transferAvailabilityCall");
	public static RequestType transferProgramAvailabilityCall 				= new RequestType("transferProgramAvailabilityCall");
	public static RequestType transferProgramContentCall 		= new RequestType("transferProgramContentCall");
	public static RequestType transferReservationCall 			= new RequestType("transferReservationCall");
	public static RequestType transferCancelationCall 			= new RequestType("transferCancelationCall");

	public static RequestType airSearchCall 				= new RequestType("airSearchCall");

	
	

}
