package comon.datatypes;

public enum TestMethod {
	
	Perfomance,Validations,Normal,NONE;
	
	public static TestMethod getType(String key)
	{
	if ( key.equals("Perf"))                return Perfomance;
	else
	if ( key.equals("Validations"))               return Validations ;
    else 
    if ( key.equals("Normal"))                    return Normal;
    else                                          return NONE  ;
    }
}
