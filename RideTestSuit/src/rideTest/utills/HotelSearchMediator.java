package rideTest.utills;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import ride.common.datatypes.RoomOccupant;

import comon.datatypes.SearchObjectType;

public class HotelSearchMediator {
	
	private SearchObjectType   Search            = null;
	private Map<String,Double> profitList        = null;
	private Map<String, String>CityCodes         = null;
	private String             Portalname        = ""  ;
	private ArrayList<String>  EnabledVendors    = null;
	private Object[][]         markups 	         = null;
	private double[] 	       markup 	         = new double[16];
	private String[][] 	       vendors 	         = null;
	private int                VendorCount       = 0;
	
	
	public HotelSearchMediator(SearchObjectType Object,Map<String,Double> profList,Map<String, String> cityCodes)
	{
		this.Search     = Object;
		this.profitList = profList;
		this.CityCodes  = cityCodes ;
		Portalname      = Search.getPortalName();
		
	}
	
	
	public SearchObjectType synchronizeAdditionalData()
	{
		
		String VendorList = PG_Properties.getProperty(Portalname+"_Suppliers");
		EnabledVendors    = new ArrayList<String>(Arrays.asList(VendorList.split(",")));
		Search.setVendorList(EnabledVendors);
		VendorCount       = EnabledVendors.size();
		this.setProfit();
		this.setVendors();
		this.setCityCode();
		this.setRoomOccupancy();
		
		
		return Search;
		
        
	}
	
	public void setProfit()
	{
		markups = new Object[VendorCount][2];
		
		for(int i=1 ; i<=VendorCount ; i++)
		{
			String VendorId   = EnabledVendors.get(i-1);
			Double vendorMarkUp = profitList.get(VendorId);
			
			for(int K = 0 ; K < 16 ; K++)  markup[K]  = vendorMarkUp;

			markups[i-1][0] = VendorId;
			markups[i-1][1] = markup;
			
		}
		
		Search.setMarkups(markups);
	}
	
	public void setVendors()
	{
		vendors = new String[VendorCount][2];
		
		for(int i=1 ; i<=VendorCount ; i++)
		{
			String VendorId   = EnabledVendors.get(i-1);
			vendors[i-1][0] = VendorId;
			vendors[i-1][1] = "1";
		}
		
		Search.setVendors(vendors);
	}
	
	
	public void setCityCode()
	{
		String CompleteCityCode = CityCodes.get(Search.getCityName());
		String[] Codes          = CompleteCityCode.split("-");
		Search.setCityCode(Codes[0]);
		Search.setCountryCode(Codes[1]);
		
		try {
			Search.setState(Codes[2]);
		} catch (Exception e) {
			Search.setState("36");
		}
		
		try {
			Search.setSuburbcode(Codes[3]);
		} catch (Exception e) {
			Search.setSuburbcode(null);
		}
	}
	
	
	public void setRoomOccupancy()
	{
		int TotnoOfAdults    = 0;
		int TotnoOfChildrens = 0;
		
		RoomOccupant[] roomOccupants = new RoomOccupant[Integer.parseInt(Search.getNOR())];
		
		for(int j=0;j<Integer.parseInt(Search.getNOR());j++){
			
		roomOccupants[j] = new RoomOccupant();		
		roomOccupants[j].setRoomNo((j+1));	


		if(j == 0 )roomOccupants[j].setAdultCount(Integer.parseInt(Search.getRoom1AD()));
		if(j == 1 )roomOccupants[j].setAdultCount(Integer.parseInt(Search.getRoom2AD()));
		if(j == 2 )roomOccupants[j].setAdultCount(Integer.parseInt(Search.getRoom3AD()));
		if(j == 3 )roomOccupants[j].setAdultCount(Integer.parseInt(Search.getRoom4AD()));

		if(j == 0 ) roomOccupants[j].setChildCount(Integer.parseInt(Search.getRoom1CH()));
		if(j == 0 ) roomOccupants[j].setChildAges(Search.getRoomOneChildAges().replace(";", ","));		
		
		if(j == 1 ) roomOccupants[j].setChildCount(Integer.parseInt(Search.getRoom2CH()));
		if(j == 1 ) roomOccupants[j].setChildAges(Search.getRoomTwoChildAges().replace(";", ","));
		
		if(j == 2 ) roomOccupants[j].setChildCount(Integer.parseInt(Search.getRoom3CH()));
		if(j == 2 ) roomOccupants[j].setChildAges(Search.getRoomThreeChildAges().replace(";", ","));
		
		if(j == 3 ) roomOccupants[j].setChildCount(Integer.parseInt(Search.getRoom4CH()));
		if(j == 3 ) roomOccupants[j].setChildAges(Search.getRoomFourChildAges().replace(";", ","));
		
		//roomOccupants[j].setRoomNo(j);
		roomOccupants[j].setOccupantFirstName("TESTMonitor");
		roomOccupants[j].setOccupantLastName("TESTLast");
		
		if(j == 0 )TotnoOfAdults += Integer.parseInt(Search.getRoom1AD());
		if(j == 1 )TotnoOfAdults += Integer.parseInt(Search.getRoom2AD());
		if(j == 2 )TotnoOfAdults += Integer.parseInt(Search.getRoom3AD());
		if(j == 3 )TotnoOfAdults += Integer.parseInt(Search.getRoom4AD());
		
		if(j == 0 )TotnoOfChildrens += Integer.parseInt(Search.getRoom1CH());
		if(j == 1 )TotnoOfChildrens += Integer.parseInt(Search.getRoom2CH());
		if(j == 2 )TotnoOfChildrens += Integer.parseInt(Search.getRoom3CH());
		if(j == 3 )TotnoOfChildrens += Integer.parseInt(Search.getRoom4CH());
		
		
		
		}
		
		Search.setRoomOccupants(roomOccupants);
		Search.setTotnoOfAdults(TotnoOfAdults);
		Search.setTotnoOfChildrens(TotnoOfChildrens);
		
	}
	
	
	
	
	

}
