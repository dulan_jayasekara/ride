/**
Dulan
 */
package rideTest.utills;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import comon.datatypes.SearchObjectType;
import comon.datatypes.TestMethod;
import comon.datatypes.TestType;

public class InfoLoader {
	
	
    private static Logger infoLogger                            = Logger.getLogger(InfoLoader.class.getName());
    private Map<Integer, String> RideSearchDetailsMap           = new HashMap<Integer, String>();
	private Map<Integer, String> RideProfitDetailsMap           = new HashMap<Integer, String>();
	private Map<Integer, String> CityDetailMap                  = new HashMap<Integer, String>();
/*	private Map<Integer, String> RatesDetailMap                 = new HashMap<Integer, String>();
	private Map<Integer, String> UrlMap                         = new HashMap<Integer, String>();*/



	public InfoLoader(ArrayList<Map<Integer, String>>  list)
	{
		infoLogger.info("Initializing info Loader..");

		try {
			infoLogger.debug("Getting the first sheet---->"+list.get(0));
			RideSearchDetailsMap      = list.get(0);    
			infoLogger.debug("Getting the Second sheet---->"+list.get(1));
			RideProfitDetailsMap      = list.get(1);  
			infoLogger.debug("Getting the Third sheet---->"+list.get(2));
			CityDetailMap             = list.get(2);   
/*			infoLogger.debug("Getting the fourth sheet---->"+list.get(3));
			RatesDetailMap            = list.get(3);   
			infoLogger.debug("Getting the fifth sheet---->"+list.get(4));
			RatesDetailMap            = list.get(4);
			infoLogger.debug("Getting the fifth sheet---->"+list.get(4));
			UrlMap                    = list.get(5);
*/
		} catch (Exception e) {

			infoLogger.fatal("Error When Adding Sheet Details");
			infoLogger.fatal(e.toString());
		
		}


	}


	public ArrayList<SearchObjectType>  getSearchList() throws ParseException{

		ArrayList<SearchObjectType> searchlist = new ArrayList<SearchObjectType>();  
		Iterator<Map.Entry<Integer,String>> it = RideSearchDetailsMap.entrySet().iterator();
		
		infoLogger.debug("------------Iterating through search Sheet------------");

		while (it.hasNext()) {
			SearchObjectType searchObj = new SearchObjectType();
			Map.Entry<java.lang.Integer, java.lang.String> entry = (Map.Entry<java.lang.Integer, java.lang.String>) it.next();
			String[] SearchDetails                               =  entry.getValue().split(",");

			infoLogger.debug("Setting the value of  reservation search record--->"+entry.getKey());
			
			try {
				searchObj.setTestMethod(TestMethod.getType(SearchDetails[0]));
			    searchObj.setCityName(SearchDetails[1]);
				searchObj.setCheckInDate(SearchDetails[2]);
				searchObj.setNOR(SearchDetails[3]);
				searchObj.setNON(SearchDetails[4]);
				searchObj.setRoom1AD(SearchDetails[5]);
				searchObj.setRoom1CH(SearchDetails[6]);
				searchObj.setRoomOneChildAges(SearchDetails[7]);
				searchObj.setRoom2AD(SearchDetails[8]);
				searchObj.setRoom2CH(SearchDetails[9]);
				searchObj.setRoomTwoChildAges(SearchDetails[10]);
				searchObj.setRoom3AD(SearchDetails[11]);
				searchObj.setRoom3CH(SearchDetails[12]);
				searchObj.setRoomThreeChildAges(SearchDetails[13]);
				searchObj.setRoom4AD(SearchDetails[14]);
				searchObj.setRoom4CH(SearchDetails[15]);
				searchObj.setRoomFourChildAges(SearchDetails[16]);
				searchObj.setPortalName(SearchDetails[17]);
				searchObj.setConnectionSerever(SearchDetails[18]);
				searchObj.setOrder(TestType.getType(SearchDetails[19]));
				searchObj.setTestType(TestType.getType(SearchDetails[20]));
	
				
			} catch (Exception e) {
				infoLogger.fatal("Error when the inserting values of reservation record--->"+entry.getKey());
			}
			


			searchlist.add(searchObj);
		}
		infoLogger.debug("------------Iterating through search Sheet Completed------------");
		return  searchlist;
	}


	public Map<String, Double>  getProfitList() throws ParseException{

		Map<String, Double>  ProfitMap                        = new HashMap<String, Double>();
		Iterator<Map.Entry<Integer,String>> ProfitIterator    = RideProfitDetailsMap.entrySet().iterator();
		/*Iterator<Map.Entry<Integer,String>> RoomIterator = RoomTypeMap.entrySet().iterator();
		Iterator<Map.Entry<Integer,String>> invenIteratoryIterator = InventoryDetailMap.entrySet().iterator();
		Iterator<Map.Entry<Integer,String>> RateIterator = RatesDetailMap.entrySet().iterator();*/


		while(ProfitIterator.hasNext())
		{
			Map.Entry<java.lang.Integer, java.lang.String> entry = (Map.Entry<java.lang.Integer, java.lang.String>) ProfitIterator.next();
			String[] ProfitDetails                               =  entry.getValue().split(",");
			
			try {
			Double Profit   = Double.parseDouble(ProfitDetails[1]);
			ProfitMap.put(ProfitDetails[0], Profit);
				
			} catch (Exception e) {
				infoLogger.fatal("Error"+e.toString()); 
			}
			
		}
		
		infoLogger.info("=================================="); 
		infoLogger.info("Returning Hotel Map--->"+ ProfitMap);
	    return ProfitMap;

	}


	public Map<String, String>  getCityList() throws ParseException{

		Map<String, String>  CityMap                          = new HashMap<String, String>();
		Iterator<Map.Entry<Integer,String>> CityIterator      = CityDetailMap.entrySet().iterator();
		/*Iterator<Map.Entry<Integer,String>> RoomIterator = RoomTypeMap.entrySet().iterator();
		Iterator<Map.Entry<Integer,String>> invenIteratoryIterator = InventoryDetailMap.entrySet().iterator();
		Iterator<Map.Entry<Integer,String>> RateIterator = RatesDetailMap.entrySet().iterator();*/


		while(CityIterator.hasNext())
		{
			Map.Entry<java.lang.Integer, java.lang.String> entry = (Map.Entry<java.lang.Integer, java.lang.String>) CityIterator.next();
			String[] CityDetails                                 =  entry.getValue().split(",");
			
			try {
		
			  CityMap.put(CityDetails[0], CityDetails[1]);
				
			} catch (Exception e) {
				infoLogger.fatal("Error"+e.toString()); 
			}
			
		}
		
		infoLogger.info("=================================="); 
		infoLogger.info("Returning Hotel Map--->"+ CityMap);
	    return CityMap;

	}








}
