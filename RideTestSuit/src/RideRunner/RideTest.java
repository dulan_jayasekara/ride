package RideRunner;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Queue;
import java.util.Random;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import rideTest.utills.PG_Properties;


import ride.services.connectorservices.RezException;
import ride.test.*;


import rezg.ride.car.server.datatypes.Vehicle;
import ride.common.datatypes.Address;
import ride.common.datatypes.Contact;
import ride.common.datatypes.CreditCard;
import ride.common.datatypes.DailyRate;
import ride.common.datatypes.ExchangeRateMethodType;
import ride.common.datatypes.Hotel;
import ride.common.datatypes.HotelList;
import ride.common.datatypes.PartnerLevelType;
import ride.common.datatypes.Promotion;
import ride.common.datatypes.RIDEErrors;
import ride.common.datatypes.Rate;
import ride.common.datatypes.Room;
import ride.common.datatypes.RoomOccupant;
import ride.common.datatypes.Tax;
import ride.common.datatypes.VendorHotel;
import ride.common.hotelavailability.AvailabilityRequest;
import ride.common.hotelavailability.AvailabilityResponse;
import ride.common.prereservationcheck.PreReservationResponse;
import ride.common.reservation.ReservationResponse;
import ride.common.services.RIDEServer;
import ride.common.utilities.ConnectionConfig;
import rideTest.utills.HotelSearchMediator;
import rideTest.utills.InfoLoader;
import rideTest.utills.ReadExcel;
import rideTest.utills.WriteToFile;
import comon.datatypes.*;



/**
 * @param args
 * @author Dulan
 * 
 */


public class RideTest {
	
	
	private ArrayList<SearchObjectType>   SearchList     = new ArrayList<SearchObjectType>();
	private Map<String, Double>           ProfitMap       = new HashMap<String, Double>();
	private Map<String, String>           CityMap        = new HashMap<String, String>();
    private ReadExcel                             Excel  ;
	private InfoLoader                            loadInfo ;
    private StringBuffer                          ReportPrinter;
    private AvailabilityResponse                  response;
    private HotelList hotels                      = null;
    private ArrayList errors                      = null;
    private Map<String, String> SupplierStatus    = null;
/*	private IncomingResponse                      AvailableResponse;
	private GenerateRequest                       RequestGenerator = null;
	private ResponseValidator                     Validator;
	*/
    private String Status                           ="ERR";
	private static Logger logger 					= null;	
	private static Logger Perflogger 			    = null;	
	private String TracerID                         = null;
	String name                                     = null;
	String id			                            = null;
	private ConnectionConfig conf                   = null;
	private String ResponseChunk                    = null;
	

	
	//20142//60001//20137//82001 Kuala Lumpur //16005//906 - London // 849 las vagas//6006//15004 Chicago  //4166 paris
	
	@Before

	public void setUp() throws ParseException, IOException
	{
	WriteToFile.Writetofile();
	ReportPrinter        = new StringBuffer();
	SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	
	ReportPrinter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> </head>");
	ReportPrinter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>Bonotel Dynamic Rates Automation Report("+sdf.format(Calendar.getInstance().getTime())+")</p></div>");
	ReportPrinter.append("<body>");

	logger        = org.apache.log4j.Logger.getLogger(this.getClass());
	Perflogger    = org.apache.log4j.Logger.getLogger(this.getClass());

	
	Excel         = new ReadExcel();
/*	Perflogger.info("elaaaaaaa");
	logger.info("Initializing the ExcelReader");*/
	ArrayList<Map<Integer,String>> list = Excel.init(PG_Properties.getProperty("Excel.Path"));
	loadInfo      = new InfoLoader(list);


	try {
		SearchList    = loadInfo.getSearchList();
		ProfitMap     = loadInfo.getProfitList();
		CityMap       = loadInfo.getCityList();

	} catch (Exception e) {
		System.out.println(e.toString());
		logger.fatal("Error With Loading object Details");
		logger.fatal(e.toString());
	
	}

	}
	
	
	public void InitalizeRideTest() throws Exception
	{
		   ConnectionConfig conf = ConnectionConfig.getInstance();	
		   System.setProperty("javax.net.ssl.trustStore",PG_Properties.getProperty("KeyStore.Path"));
		   System.setProperty("javax.net.ssl.trustStorePassword","123456");
		
	}
	
	
	
	@Test
	public void RideTestRunner() throws Exception {
		Iterator<SearchObjectType> searchIterator = SearchList.iterator();

		while (searchIterator.hasNext()) {
		    ResponseChunk                    = " ";
			runtest(searchIterator.next());
		}
	}

	@Test
	public void PerfRunner1() throws Exception {
		
		runtest(SearchList.get(0));
	}

	@Test
	public void PerfRunner2() throws Exception {
		runtest(SearchList.get(1));
	}

	@Test
	public void PerfRunner3() throws Exception {
		runtest(SearchList.get(2));
	}

	@Test
	public void PerfRunner4() throws Exception {
		runtest(SearchList.get(3));
	}

	@Test
	public void PerfRunner5() throws Exception {
		runtest(SearchList.get(4));
	}


@After

public void tearDown()
{
	
}


public void runtest(SearchObjectType currentSearch) throws RezException
{
	   ResponseChunk                    = " ";
       HotelSearchMediator Mediator   = new HotelSearchMediator(currentSearch,ProfitMap,CityMap);
	   currentSearch                  = Mediator.synchronizeAdditionalData();
	   conf.setRideServerIp(currentSearch.getConnectionSerever());

	    Queue<TestType >  TestOrder      = currentSearch.getOrder();
	   
	   while (!TestOrder.isEmpty()) {
	
	   TestType CurrentTest  = TestOrder.poll();
	   
	   if(CurrentTest == TestType.AVAILABILITY)
	   {
			AvailabilityRequest request = new AvailabilityRequest();
			id 				= "RIDE_"+(int)(Math.random()*100000);
			request.setId(id);
			
			if (currentSearch.getCacheKey() != null) {
				request.setCacheKey(currentSearch.getCacheKey());
				request.setSearchType(1);
				request.setRequestType(1);
				request.setMarkUpList(currentSearch.getMarkups());
			} else{
				//If the Serach data is not in  the Cache tables taking from the suppliers
			
				
				//Set the request Data
				request.setCheckInDate(currentSearch.getCheckInDate());
				request.setCheckOutDate(currentSearch.getCheckOutDate());
				request.setNoOfAdults(currentSearch.getTotnoOfAdults());
				request.setCurrentVendorOrder(1);
				request.setNoOfChildren(currentSearch.getTotnoOfChildrens());
				request.setNoOfRooms(currentSearch.getNON());
				request.setCountry(currentSearch.getCountryCode()); 
				request.setCity(currentSearch.getCityCode());
				request.setSuburb(currentSearch.getSuburbcode());
				request.setState(currentSearch.getState());
				request.setPortal(currentSearch.getPortal());
				request.setStatus("F");
				request.setMarkUpList(currentSearch.getMarkups());
				request.setVendorList(currentSearch.getVendors());
				request.setSortedBy(3);
				request.setRoomOccupants(currentSearch.getRoomOccupants());
				request.setStart(0);
				request.setNoOfResults(9999);
				request.setSessionId("99");
				request.setPartnerLevel(PartnerLevelType.B2C);
				request.setExchangeRateMethod(ExchangeRateMethodType.sellingRate);
				//request.setRequestCurrencyType(CurrencyCodeType.KWD);
				request.setLanguageCode("en");
				
				//request.setRequestMethod("JSON");
				//request.setRequestPackage("REZG");
				//request.setRequestPackage("RIDE");
				
				//request.setCombineRooms(true);
				//request.setCombineHotels(true);
				
		
				
				Properties prop = new Properties();
				prop.put("HotelCode","0");
				prop.put("HOTEL_CODE","0");
				prop.put("HOTEL_ATTRACTIONID","ALL");
				prop.put("HOTEL_LOCID","ALL");
				prop.put("BED_TYPE_ID_0","%" );
				prop.put("HOTEL_GROUPID","ALL");
				prop.put("LANGUAGE_CODE","en");
				prop.put("HOTEL_STARID","ALL");
				prop.put("NIGHTS","6");
				prop.put("ROOM_TYPE_ID_0","%");
				prop.put("RES_SOURCE","WEB");
				prop.put("USER_CODE","0");
				prop.put("CURRENCY","AUD");
				prop.put("HOTEL_TYPEID","ALL");
				prop.put("USER_TYPE","WEB");
				prop.put("TOUR_OPR_ID","0");
				prop.put("customercountry","LK");

				request.setVendorSpecificProperties(prop);
				
			}
			
			ride.services.connectorservices.ServiceData rideServiceData = new ride.services.connectorservices.ServiceData();
			//System.out.println("Requessssssssttttttt"+request.toString());
			rideServiceData.setServiceName("RIDEService");
			rideServiceData.setSchemaName(currentSearch.getSchema());
			rideServiceData.setModuleName("hotels");
			rideServiceData.setActionMethod("");
			rideServiceData.setInputData(request);
			RIDEServer rideServer = new RIDEServer();
			
			Long Currentmillies = System.currentTimeMillis();
			rideServer.doService(rideServiceData);
			Long Aftermillies   = System.currentTimeMillis();
			Long ElapsedTime    = (Aftermillies - Currentmillies)/1000;


		
			
			
			try {
				
				response = (AvailabilityResponse) rideServiceData
						.getOutputData(0);

				
				hotels = (HotelList) response.getHotelList();
				errors = (ArrayList) response.getErrors();
				
			} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
				List errors = new ArrayList(1);
				response.setErrors(errors);
			}
			
			
			
	
			if (errors.size() > 0) {
				for (int i = 0; i < errors.size(); i++) {
					logger.info("Error Code - >"+((RIDEErrors)response.getErrors().get(i)).getErrorCode());
					logger.info("Error Desc - >"+((RIDEErrors)response.getErrors().get(i)).getError());
				}
			}
			
			String  To                 = currentSearch.getCityName();
			String  Date               = new SimpleDateFormat("MM-dd-yyyy").format(currentSearch.getCheckInDate());
			String VendorHotelCount    = "0";
			
		try {
			
			String SupplierResponsetime = "";
			SupplierStatus             = response.getSupplierStatus();
			
			for (String Supplier : currentSearch.getVendorList()) {
				
				try {
					SupplierResponsetime = SupplierStatus.get(Supplier+"_ResponceTime");
				} catch (Exception e) {
					SupplierResponsetime = "Err" ;
				}
				ResponseChunk  =ResponseChunk + Supplier+"="+SupplierResponsetime+" ";
				//ResponseChunk.concat(Supplier+"="+SupplierResponsetime+" ");
			}
			
			VendorHotelCount           = SupplierStatus.get("vhotelcount");
			VendorHotel CurrentVHotel  = hotels.getVendorHotel().get(0);
			Hotel       CurrentHotel   = CurrentVHotel.getVendorHotel();
			TracerID                   = (String)CurrentHotel.getVendorHotelData("traceId");
			if(Integer.parseInt(VendorHotelCount)> 0)
			Status   = "Avail";
			else
			Status   = "Fail";
			
			ArrayList<String>  RideList = readLog(TracerID);
			
			
			Perflogger.info(TracerID+" "+To+" "+Date+" "+Status+" "+"tooktime="+ElapsedTime +ResponseChunk+" "+"Count="+VendorHotelCount+ " "+ RideList);
			
		} catch (Exception e) {
			Perflogger.info(TracerID+" "+To+" "+Date+" "+Status+" "+"tooktime="+ElapsedTime +ResponseChunk+" "+"Count="+VendorHotelCount+ " ");
		}

	
			
	   }
		
		
	}
	   	
}


public ArrayList<String>  readLog(String TracerId)
{
	
	String Date = new SimpleDateFormat("yyyy_dd_MM").format(Calendar.getInstance().getTime());
	Document doc = null;
	ArrayList<String> outputMap = new ArrayList<String>();
    
try {
	 doc = Jsoup.connect(PG_Properties.getProperty("Log.Url".replace("DATE", Date))).get();
} catch (Exception e) {
	// TODO: handle exception
}

try {
		Elements  elements = doc.getElementsContainingText("seconds tracer id: "+TracerID);
		
		for (Element element : elements) {
			outputMap.add(element.text().replace("seconds tracer id: "+TracerID, ""));
			}
	} catch (Exception e) {
		// TODO: handle exception
	}

	return outputMap;
}

	
}
