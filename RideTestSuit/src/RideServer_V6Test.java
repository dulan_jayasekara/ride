

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import rezg.dto.hotels.HotelContentDTO;
import rezg.ride.car.server.content.ContentRequest;
import rezg.ride.car.server.content.ContentResponse;
import rezg.ride.car.server.datatypes.Charge;
import rezg.ride.car.server.datatypes.CustTelephoneInfo;
import rezg.ride.car.server.datatypes.CustomerInfo;
import rezg.ride.car.server.datatypes.IteneraryInfo;
import rezg.ride.car.server.datatypes.Rental;
import rezg.ride.car.server.datatypes.TravelItenerary;
import rezg.ride.car.server.datatypes.Traveler;
import rezg.ride.car.server.datatypes.Vehicle;
import rezg.ride.car.server.datatypes.Vendor;
import rezg.ride.car.server.datatypes.VendorResponse;
import ride.common.cancellation.CancellationRequest;
import ride.common.cancellation.CancellationResponse;
import ride.common.datatypes.Address;
import ride.common.datatypes.CancellationPolicy;
import ride.common.datatypes.CancellationPolicyCharge;
import ride.common.datatypes.Contact;
import ride.common.datatypes.CreditCard;
import ride.common.datatypes.CurrencyCodeType;
import ride.common.datatypes.DailyRate;
import ride.common.datatypes.ExchangeRateMethodType;
import ride.common.datatypes.ExchangeRateType;
import ride.common.datatypes.Hotel;
import ride.common.datatypes.HotelDataFilterType;
import ride.common.datatypes.HotelList;
import ride.common.datatypes.Language;
import ride.common.datatypes.NearByAttraction;
import ride.common.datatypes.NearByAttractionGourp;
import ride.common.datatypes.PartnerLevelType;
import ride.common.datatypes.Promotion;
import ride.common.datatypes.RIDEErrors;
import ride.common.datatypes.Rate;
import ride.common.datatypes.RideMessage;
import ride.common.datatypes.Room;
import ride.common.datatypes.RoomOccupant;
import ride.common.datatypes.SystemTrace;
import ride.common.datatypes.Tax;
import ride.common.datatypes.VendorHotel;
import ride.common.hotelavailability.AvailabilityRequest;
import ride.common.hotelavailability.AvailabilityResponse;
import ride.common.hotelcontent.HotelContentRequest;
import ride.common.hoteldata.HotelDataRequest;
import ride.common.hoteldata.HotelDataResponse;
import ride.common.nearbyattraction.NearByAttractionsRequest;
import ride.common.nearbyattraction.NearByAttractionsResponse;
import ride.common.prereservationcheck.PreReservationRequest;
import ride.common.prereservationcheck.PreReservationResponse;
import ride.common.reservation.ReservationRequest;
import ride.common.reservation.ReservationResponse;
import ride.common.roomavailability.RoomAvailabilityRequest;
import ride.common.roomavailability.RoomAvailabilityResponse;
import ride.common.services.RIDEServer;
import ride.common.utilities.ConnectionConfig;
import ride.currency.services.RIDECurrencyServer;
import ride.services.connectorservices.RezException;
import ride.services.connectorservices.ServiceData;
import ride.trace.services.RIDETraceServer;


public class RideServer_V6Test implements Runnable{

	/**
	 * @param args
	 * @author sameera
	 * @Test Class For Most of the Suppler Interfaces ;)
	 */

	
	private static Logger logger 					= null;	
	String name = null;
	
	String id			= null;

	String vendorid 	= null;
	String schema    	= null;
	
	static Date checkInDate    = new Date();
	static Date checkOutDate    = new Date();
	
	Calendar cal = Calendar.getInstance();
	String time = cal.getTime().getHours()+":"+cal.getTime().getMinutes()+":"+cal.getTime().getSeconds();

	Object[][] 	markups 	= null;
	double[] 	markup 		= new double[16];
	String[][] 	vendors 	= null;

	String  State  			= "-";
	String  countryCode  	= "54"; 
	String  cityCode    	= "-";
	String  suburbcode    	= null;
	
	int RoomOnenoOfAdults 		= 1;
	int RoomTwonoOfAdults 		= 1;
	int RoomThreenoOfAdults 	= 1;
	int RoomFournoOfAdults 	= 1;
	
	int RoomOnenoOfChildrens 	= 0;
	int RoomTwonoOfChildrens 	= 0;
	int RoomThreenoOfChildrens 	= 0;
	int RoomFournoOfChildrens 	= 0;
	
	String RoomOnenoOfChildAge 		= "";
	String RoomTwonoOfChildAge 		= "";
	String RoomThreenoOfChildAge 	= "";
	String RoomFournoOfChildAge 	= "";
	
	int numberOfRooms 	= 1;
	int TotnoOfAdults 	= 0;
	int TotnoOfChildrens 	= 0;
	
	String portal         = null;
	
	RoomOccupant[] roomOccupants = null;

	String cacheKey               = null;
	AvailabilityResponse response = null;
	HotelList hotels = null;
	
	Hotel Test2Hotel = null;  
	//rezg.dto.ride.common.datatypes.Hotel Test2Hotelrezg = new rezg.dto.ride.common.datatypes.Hotel(); 
	ArrayList errors              = null;
	
	String testHotelCode  = null;
	String testHotelName  = null;
	String rezervationNo  = "REZ00123";
	
	ReservationResponse resp = null;
	String[] iteno                                    = null;
	String  resNum []                                 = null; 
	Hotel roomAvailabilityHotel                       = null;
	Room  roomAvailabilityRoom                        = null;
	Hotel preReservationHotel                         = null;
	Room  preReservationRoom                          = null;
	Rate  preReservationRate                          = null;
	PreReservationResponse preReservationResponse     = null;
	RoomOccupant  preReservationRoomOccupant          = null;
	DailyRate     preReservationDailyRates            = null;
	Tax           preReservationTax                   = null;
	Contact[] contact                                 = null;
	Address[] address                                 = null;
	CreditCard card                                   = null;
	int count                                         = 0;
	String SelectedRoomCode							  = null;
	
	String pickUpDateTime = null;
	String returnDateTime = null;
	
	String pickUpLocationCode = null;
	String returnLocationCode = null;
	
	Properties	vendorProps = null;
	
	Vehicle resVehicleObj = null;
	
	static ArrayList<AvailabilityResponse> searchResArray 	  = new ArrayList<AvailabilityResponse>();
	
	static testType			testType				= null;
	
	//20142//60001//20137//82001 Kuala Lumpur //16005//906 - London // 849 las vagas//6006//15004 Chicago  //4166 paris
	

	
	public RideServer_V6Test(ArrayList<AvailabilityResponse> searchResArray) throws Exception{
		logger = Logger.getLogger(getClass()); 
		ConnectionConfig conf = ConnectionConfig.getInstance();	
//		conf.setRideServerIp("localhost");
		//conf.setRideServerIp("192.168.1.135:9090");
	//	conf.setRideServerIp("192.168.1.126:9090");
	    conf.setRideServerIp("ride-test.secure-reservation.com"); //---- STG Servrt (MC6)
//		conf.setRideServerIp("ride.secure-reservation.com"); //------ Live Server (MC6)
//		conf.setRideServerIp("ride-backup.secure-reservation.com:81"); //------ Live Server  (MCL13)
	}

	public void setup() throws Exception {
		
		System.out.println("SAAEAEA");
//		System.exit(0);
		
		testType			= testType.hotelSearchCall;
//		testType			= testType.hotelRoomSearchCall;
//		testType			= testType.hotelReservationCall;
//		testType			= testType.hotelCancelationCall;
//		testType			= testType.hotelContent;
		
		//testType			= testType.carSearchCall; 
		//testType			= testType.carContentCall;
		//testType			= testType.carPreReservationCall;
		//testType			= testType.carReservationCall;
		//testType			= testType.carCancelationCall;
		
		//testType			= testType.transferAvailabilityCall;
		//testType			= testType.transferProgramAvailabilityCall;
		//testType			= testType.transferProgramContentCall;
		//testType			= testType.transferReservationCall;
		//testType			= testType.transferCancelationCall;
		
		
		//testType			= testType.airSearchCall;
		
		vendorid 			= "hotelbeds_v2";             
		
    	SimpleDateFormat   sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");    	
    	
        checkInDate    					= sdf.parse("2014-03-11T09:00:00");
        checkOutDate      				= sdf.parse("2014-03-15T09:00:00");
        
        RoomOnenoOfAdults 		= 1;
        RoomTwonoOfAdults 		= 1;
        RoomThreenoOfAdults 	= 0;
        RoomFournoOfAdults 		= 0;
        
        RoomOnenoOfChildrens 	= 1;
        RoomTwonoOfChildrens 	= 2;
        RoomThreenoOfChildrens 	= 0;        
        RoomFournoOfChildrens 	= 0;  
        
        RoomOnenoOfChildAge 	= "5";
    	RoomTwonoOfChildAge 	= "6,7";
    	RoomThreenoOfChildAge 	= "";
    	RoomFournoOfChildAge 	= "";
        
    	numberOfRooms     		= 1;
    	
    	cityCode    			= "1112"; // 1112 London , 530 Paris , 809 Amsterdam , 739 Mauritius //  553 //Athens
    	State    				= "36";
    	schema    				= "rezbase_v3";	 //rezbase_v3,arabiahorizons,bonotel
    	countryCode    			= "54";
        
	
    	pickUpDateTime = sdf.format(checkInDate);
    	returnDateTime = sdf.format(checkOutDate);
    	
    	pickUpLocationCode = "AMS";
    	returnLocationCode = "AMS";
    	
    	double markUp = 0.1;
    	
		markup[0] = markUp;
		markup[1] = markUp;
		markup[2] = markUp;
		markup[3] = markUp;
		markup[4] = markUp;
		markup[5] = markUp;
		markup[6] = markUp;
		markup[7] = markUp;
		markup[8] = markUp;
		markup[9] = markUp;
		markup[10] = markUp;
		markup[11] = markUp;
		markup[12] = markUp;
		markup[13] = markUp;
		markup[14] = markUp;
		markup[15] = markUp;
    	
	}
	
	public void config()throws Exception {
			
			System.setProperty("javax.net.ssl.trustStore","C:/rezsystem/jboss-4.0.3SP1/jboss-4.0.3SP1/bin/certs/QA.keystore");
	        System.setProperty("javax.net.ssl.trustStorePassword","123456");
	        
	        

		
	        // Common Vendor Settings
	    	
	        id 				= "RIDE_"+(int)(Math.random()*100000);
	    	
	    	// Specific Vendor Settings
	        if(vendorid.equals("blumar")){
	        	
	        schema    			= "rezbase_v3";	        
	    	testHotelCode  	= "roya";
	    	SelectedRoomCode   = "000000856_Sgl_1_1";	    	
	    	
	        }else if(vendorid.equals("tourico")){
	        	
		        //schema    			= "rezbase_v3";	        
		    	testHotelCode  	= "roya";
		    	SelectedRoomCode   = "000000856_Sgl_1_1";	   
		    	//cityCode    		= "1350"; //new york
		    	//cityCode    		= "809";  //Amsterdam
		    	//cityCode    		= "1210";  //paris
		    	
	        }else if(vendorid.equals("jactravel")){
	        	
		        schema    			= "rezbase_v3";	        
		        testHotelCode  	= "6026";
		        SelectedRoomCode   = "0_99";
		        
	        }else if(vendorid.equals("mondial")){
	        	
	        	cityCode    		= "32"; //Vienna
	        	schema    			= "alshamel";	        
	        	  
	        }else if(vendorid.equals("hotelclub")){
	        	
	        	
	        	
	        }else if(vendorid.equals("dotw")){
	        	
		        //schema    			= "saudiworldholidays";	 
		        cityCode    		= "1112";
		    	testHotelCode  	= "32814";
		    	SelectedRoomCode   = "65374_1331_0";	   
		    	 schema    			= "rezbase_v3";	   
														    	
													        	}else if(vendorid.equals("hotelspro")){
															        schema    			= "rezbase_v3";	   
															        testHotelCode  	= "US9WAP";
															        cityCode    		= "849";															        
			}else if(vendorid.equals("hotelpronto_v1")){
				schema    			= "rezbase_v3";	        
				cityCode    		= "950";
				testHotelCode  	= "147056183";
				SelectedRoomCode   = "1338204";
				
			}else if(vendorid.equals("procan")){
				
				cityCode    		= "1";
			    schema    			= "banamex";	  
			    
			}else if(vendorid.equals("kobra")){
				
	        	//schema    			= "travelwinds";
				//cityCode    		= "4";
				//countryCode    	= "53";
				
	        	testHotelCode  	= "3953";
	        	SelectedRoomCode   = "3_2_2";

			}else if(vendorid.equals("specialtours")){
        
				//cityCode    		= "1210";
				//countryCode    	= "214";
				
			}else if(vendorid.equals("tripserve")){
				

				testHotelCode  	= "97670";
				SelectedRoomCode   = "SGL-U02_CONTINENTAL BREAKFAST_ST_1";

			}else if(vendorid.equals("altayyardubai")){
				
				schema    			= "altayyarsaudi";	
				testHotelCode  	= "496";
				SelectedRoomCode   = "3734_59_9_1";
				
		    	cityCode    		= "3086";
		    	//countryCode    	= "30";
		    	
			}else if(vendorid.equals("lenatours")){
				
				schema    			= "altayyaregypt";	
				testHotelCode  	= "5";
				SelectedRoomCode   = "16_3_2_1";
				
		    	cityCode    		= "3086";
		    	countryCode    	= "30";
		    	
			}else if(vendorid.equals("altayyaregypt")){
				
				schema    			= "altayyardubai";	
				testHotelCode  	= "17";
				SelectedRoomCode   = "16_3_2_1";
				
		    	cityCode    		= "3086";
		    	//countryCode    	= "30";
		    	
			}else if(vendorid.equals("xmlout")){
				
				schema    			= "rezbase_v3";	
				testHotelCode  	= "5895";
				SelectedRoomCode   = "DBT-U02_ENGLISH BREAKFAST_SP_1";

			}else if(vendorid.equals("amadeus")){
				
				cityCode    		= "1286";
				schema    			= "unishore";	
				testHotelCode  	=   "AA5-IN";				
				SelectedRoomCode   = "KNG482G_482_0";
				
			}else if(vendorid.equals("hostelworld")){
				
				cityCode    		= "892";  //Poznan
				countryCode    		= "3";  //Poland
				
				testHotelCode  	=   "34234";			
				SelectedRoomCode   = "x4_MixedXDorm_0_BASIC___1,x6_MixedXDorm_0____2";
				
	        }else if(vendorid.equals("miki")){
	        	
	        	//cityCode    		= "1210"; //1031 Milan
	        	//schema    			= "rezbase_v3";
	        	//schema    			= "lamarholidays";
				//schema    			= "tripserve";
	        	//schema    			= "saudiworldholidays";
	        	//schema    			= "unishore";
	        	//schema    			= "travelwinds";
	        	
				//suburbcode    		= "3533";
	        	
	        	//cityCode    		= "809";  //Amsterdam
	        	//cityCode    		= "1210";  //paris
	        	//cityCode    		= "670";  //rome
	        	
	        	testHotelCode  	= "CAG700700";
	        	SelectedRoomCode   = "00001_1";	        
	        	
	        }else if(vendorid.equals("synxis")){
	        	
		    	cityCode    		= "739";
		    	//cityCode    		= "1336";
		    	
	        	testHotelCode  	= "11187"; 	        	
	        	SelectedRoomCode   = "XXB_1,JS_2";
	
	        	
	        	//schema    			= "acexperts";
	        	//schema    			= "accentral";
	        	schema    			= "airmauritiusholidays";
//	        	schema    			= "saudiworldholidays";
	        	//schema    			= "rezbase_v3";
	        	
	        	
	        }else if(vendorid.equals("travflex")){
		        	
		    		countryCode    	= "111"; //MY
		        	cityCode    		= "798";
		        	testHotelCode  	= "WSMA0902021189";
		        	SelectedRoomCode   = "WSMA05110039_Twin_1";
		        	
	        }else if(vendorid.equals("jonview")){      	
	        	schema    			= "altayyardubai";
	        	
	        	//cityCode    		= "1350"; // new york
	        	//cityCode    		= "1344"; // las vagas
	        	cityCode    		= "156"; // las vagas
	        	
	        	testHotelCode  	= "2141";
	        	SelectedRoomCode   = "00002_0";
	        }else if(vendorid.equals("ctntours")){      	
	        	schema    			= "altayyardubai";
	        	cityCode    		= "1350";
	        	testHotelCode  	= "2141";
	        	SelectedRoomCode   = "00002_0";
													        	}else if(vendorid.equals("gogloble")){													        		
													        		schema    			= "rezbase_v3";	
													        		testHotelCode  	= "94790-124017/27858/1";
													        		//testHotelName      = "WESTIN RESORT (OCEAN VIEW)";
													        		//cityCode    		= "85";//ARUBA
													        	}else if(vendorid.equals("hotusa")){
													        		//THIS IS THE LIVE MOOD
													        		cityCode    		= "37300";
													        		countryCode		= "373";
													        		schema    			= "rezbase_v3";	
													        		testHotelCode  	= "9841-6069/8050/932";
													        	}else if(vendorid.equals("hoojoozat")){													        		
													        		schema    			= "rezbase_v3";	
													        		//testHotelCode  	= "61929-91118/10949/1036";
			}else if(vendorid.equals("INV13")){      	
				//schema    			= "trips";
				testHotelCode  	= "1216326";
				SelectedRoomCode   = null;
				SelectedRoomCode   = "11859597_1_1,11859597_2_2";
				
				
			}else if(vendorid.equals("travco")){  
				
				//schema    			= "alshamel";
				//cityCode    		= "1112";
				testHotelCode  	= "YYL";
				SelectedRoomCode   = "SWB_1,DWB_2";
										
			}else if(vendorid.equals("INV27")){      	
				//schema    			= "tripserve";
				//schema    			= "flightsightv3";
				//schema    			= "saudiworldholidays";
				//countryCode    	= "153";
				//cityCode    		= "50";
				//suburbcode    		= "3534";
				testHotelCode  	= "TRI1_LON";
				SelectedRoomCode   = "001:TRI1_SB_0";
			}else if(vendorid.equals("hotelbeds_v2")){   
				//schema    			= "unishore";
				//schema    			= "travelwinds";
				schema    			= "alshamel";
				cityCode    		= "1112";
				testHotelCode  	= "25496";
				SelectedRoomCode   = "DBT-U02_CONTINENTAL BREAKFAST_SP_1";
				
			}else if(vendorid.equals("resclick")){    
				
				cityCode    		= "436"; // Madrid
				testHotelCode  	= "001437";
				SelectedRoomCode   = "0_1";
				schema    			= "rezbase_v3";	   
				
			}else if(vendorid.equals("tourplan")){  

				testHotelCode  	= "DUBACCAS009FBB";
				cityCode    		= "599";
				SelectedRoomCode   = "TW_1";

	        }else if(vendorid.equals("hotelsdotcom")){     
				schema    			= "rezbase_v3";
				cityCode    		= "1344";
				testHotelCode  	= "118583";
				//SelectedRoomCode   = "TPL-U02_CONTINENTAL BREAKFAST_ST_0";
	        }else if(vendorid.equals("whitesands") || vendorid.equals("whitesandsaprt")){  
	        	
				//schema    			= "alshamel";
				//schema    			= "saudiworldholidays";
	        	//schema    			= "rezbase_v6";
	        	
				cityCode    		= "4";
				countryCode    	= "53";
				testHotelCode  	= "TES";
				SelectedRoomCode   = "TES_SGL_SRB_1";
				
	        }else if(vendorid.equals("CAR1")){  
	
			}
 
	}

	public static void main(String[] args) throws Exception {
		
		System.out.println("main(). :AAAAAAAAAAAAAAAAAAAA ");
		
		System.out.println("*******************************************************************************");
//		String inputString = "PaymentRequest{Id=123456, type=CREDIT_CARD, creditCardDetails=CreditCardDetails{type=VISA, name=Some Name, number=1234567890123456, expiry=0316, CCV=000}, directDebitDetails=null}";
//				String result = inputString.replaceAll("(?=number=\\d{1,16},)(number=\\d*?)\\d{1,4},", "$1--HIDDEN--,");
//				System.out.println(result);
//				System.exit(0);
		
		//DOMConfigurator.configure("log4j.xml");
		BasicConfigurator.configure();
	
		
		int mb = 1024*1024;
         
        //Getting the runtime reference from system
        Runtime runtime = Runtime.getRuntime();
         
        System.out.println("##### Heap utilization statistics [MB] #####");
         
        //Print used memory
        System.out.println("Used Memory:"
            + (runtime.totalMemory() - runtime.freeMemory()) / mb);
 
        //Print free memory
        System.out.println("Free Memory:"
            + runtime.freeMemory() / mb);
         
        //Print total available memory
        System.out.println("Total Memory:" + runtime.totalMemory() / mb);
 
        //Print Maximum available memory
        System.out.println("Max Memory:" + runtime.maxMemory() / mb);

/*        for (int i = 0; i < 20; i++) {
        	
        	RideServer_V6Test rideServer_V6Test = new RideServer_V6Test(searchResArray);     
        	Thread thread = new Thread(rideServer_V6Test);
        	thread.start();
        	//Thread.sleep(4000);
        	
		}*/
        
				
		try{
					
		RideServer_V6Test rideServer_V6Test = new RideServer_V6Test(searchResArray);     
		rideServer_V6Test.setup();
		rideServer_V6Test.config();

		if(null == testType){
			
			//rideServer_V6Test.HotelCall();			
			//rideServer_V6Test.RoomCall();
//	        rideServer_V6Test.HotelContent();
	        //rideServer_V6Test.PreReservationCall();
	        //rideServer_V6Test.ReservationCall();
	        //rideServer_V6Test.CancelationCall();	      

			rideServer_V6Test.ExchangeRatesUpdate();
			//rideServer_V6Test.SystemTrace();
			//rideServer_V6Test.MemCached();
//			rideServer_V6Test.HotelContentDataService();	
			rideServer_V6Test.HotelDataService();
//			rideServer_V6Test.GetNearByAttractions();
			//rideServer_V6Test.RunService();

		}else if (testType.getTest().equals("hotelSearchCall")){
			rideServer_V6Test.HotelSearchCall();
		}else if (testType.getTest().equals("hotelRoomSearchCall")){
			rideServer_V6Test.HotelSearchCall();			
			rideServer_V6Test.HotelRoomSearchCall();
		}else if (testType.getTest().equals("hotelPreReservationCall")){
			rideServer_V6Test.HotelSearchCall();			
			rideServer_V6Test.HotelRoomSearchCall();
	        rideServer_V6Test.HotelPreReservationCall();
		}else if (testType.getTest().equals("hotelReservationCall")){
			rideServer_V6Test.HotelSearchCall();			
			rideServer_V6Test.HotelRoomSearchCall();
	        rideServer_V6Test.HotelPreReservationCall();
	        rideServer_V6Test.HotelReservationCall();
		}else if (testType.getTest().equals("hotelCancelationCall")){
			rideServer_V6Test.HotelCancelationCall();	     
		}else if (testType.getTest().equals("hotelContent")){
			rideServer_V6Test.HotelContentDummy();
			
		}else if (testType.getTest().equals("carSearchCall")){
//			rideServer_V6Test.CarSearchCall();
		}else if (testType.getTest().equals("carContentCall")){
//			rideServer_V6Test.CarSearchCall();
			rideServer_V6Test.CarContentCall();
		}else if (testType.getTest().equals("carPreReservationCall")){
//			rideServer_V6Test.CarSearchCall();
			rideServer_V6Test.CarPreReservationCall();
		}else if (testType.getTest().equals("carReservationCall")){
//			rideServer_V6Test.CarSearchCall();
			rideServer_V6Test.CarPreReservationCall();
			rideServer_V6Test.CarReservationCall();
		}else if (testType.getTest().equals("carCancelationCall")){
			rideServer_V6Test.CarCancelationCall();
			
			
			testType			= testType.transferAvailabilityCall;
			//testType			= testType.transferProgramAvailabilityCall;
			//testType			= testType.transferProgramContentCall;
			//testType			= testType.transferReservationCall;
			//testType			= testType.transferCancelationCall;
			
		}else if (testType.getTest().equals("transferAvailabilityCall")){
			rideServer_V6Test.TransferAvailabilityCall();
		}else if (testType.getTest().equals("transferProgramAvailabilityCall")){
			rideServer_V6Test.TransferAvailabilityCall();
			rideServer_V6Test.TransferProgramAvailabilityCall();
		}else if (testType.getTest().equals("transferProgramContentCall")){
			rideServer_V6Test.TransferAvailabilityCall();
			rideServer_V6Test.TransferProgramContentCall();
		}else if (testType.getTest().equals("transferReservationCall")){
			rideServer_V6Test.TransferAvailabilityCall();
			rideServer_V6Test.TransferProgramAvailabilityCall();
			rideServer_V6Test.TransferReservationCall();
		}else if (testType.getTest().equals("transferCancelationCall")){
			rideServer_V6Test.TransferCancelationCall();
			
		}else if (testType.getTest().equals("airSearchCall")){
			rideServer_V6Test.AirSearchCall();
		}
		
		
		
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	System.exit(0);

	}
	
	public void run() {
		// TODO Auto-generated method stub
		
		try{
			
		RideServer_V6Test rideServer_V6Test = new RideServer_V6Test(searchResArray);   
		rideServer_V6Test.setup();
		rideServer_V6Test.config();

/*		int r = (int) (Math.random() * 100 );
		
		Calendar in_cal =  Calendar.getInstance();
		Calendar out_cal =  Calendar.getInstance();
		
		in_cal.setTime(rideServer_V6Test.checkInDate);
		in_cal.add(Calendar.DATE, r);
		
		out_cal.setTime(rideServer_V6Test.checkOutDate);
		out_cal.add(Calendar.DATE, r);
		
		rideServer_V6Test.checkInDate = in_cal.getTime();
		rideServer_V6Test.checkOutDate = out_cal.getTime();	*/	
		
		AvailabilityResponse res = null;
		
		res = rideServer_V6Test.HotelSearchCall();
		
		searchResArray.add(res);
		
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}
	
	
	
	private OutputStream convert(String aString)
	{
	//Convert the string to a byte array
	byte[] byteArray = aString.getBytes();
	//Create a stream of that byte array
	ByteArrayOutputStream out = new ByteArrayOutputStream(byteArray.length);
	try
	{
	//Write the data to that stream
	out.write(byteArray);
	} catch(Exception e)
	{
	e.printStackTrace();
	}
	//Cast to OutputStream and return
	return (OutputStream) out;
	}
	
	public  AvailabilityResponse  HotelSearchCall() throws Exception {
		
		String extraVendors = null;
		
		//extraVendors = "tourico,travco,specialtours";
		//extraVendors = "hotelbeds_v2";
		
		if(null == extraVendors ){
			
			markups = new Object[1][2];
			vendors = new String[1][2];

			markups[0][0] = vendorid;
			markups[0][1] = markup;
			
			vendors[0][0] =  vendorid;
			vendors[0][1] = "1";
			
		}else{
		
			markups = new Object[extraVendors.split(",").length + 1][2];
			vendors = new String[extraVendors.split(",").length + 1][2];
			
			markups[0][0] = vendorid;
			markups[0][1] = markup;
			
			vendors[0][0] =  vendorid;
			vendors[0][1] = "1";
			
		for (int i = 1; i < extraVendors.split(",").length + 1; i++) {
			
			markups[i][0] = extraVendors.split(",")[i -1];
			markups[i][1] = markup;
			
			vendors[i][0] =  extraVendors.split(",")[i -1];
			vendors[i][1] = "1";
			
		}
		
		}
		
		
		portal         = schema+"_hotels";
		roomOccupants = new RoomOccupant[numberOfRooms];
		
		for(int j=0;j<numberOfRooms;j++){
			
		roomOccupants[j] = new RoomOccupant();		
		roomOccupants[j].setRoomNo((j+1));	
		logger.info("Room no--->"+roomOccupants[j].getRoomNo());

		if(j == 0 )roomOccupants[j].setAdultCount(RoomOnenoOfAdults);
		if(j == 1 )roomOccupants[j].setAdultCount(RoomTwonoOfAdults);
		if(j == 2 )roomOccupants[j].setAdultCount(RoomThreenoOfAdults);
		if(j == 3 )roomOccupants[j].setAdultCount(RoomFournoOfAdults);

		if(j == 0 ) roomOccupants[j].setChildCount(RoomOnenoOfChildrens);
		if(j == 0 ) roomOccupants[j].setChildAges(RoomOnenoOfChildAge);		
		
		if(j == 1 ) roomOccupants[j].setChildCount(RoomTwonoOfChildrens);
		if(j == 1 ) roomOccupants[j].setChildAges(RoomTwonoOfChildAge);
		
		if(j == 2 ) roomOccupants[j].setChildCount(RoomThreenoOfChildrens);
		if(j == 2 ) roomOccupants[j].setChildAges(RoomThreenoOfChildAge);
		
		if(j == 3 ) roomOccupants[j].setChildCount(RoomFournoOfChildrens);
		if(j == 3 ) roomOccupants[j].setChildAges(RoomFournoOfChildAge);
		
		//roomOccupants[j].setRoomNo(j);
		roomOccupants[j].setOccupantFirstName("TESTMonitor");
		roomOccupants[j].setOccupantLastName("TESTLast");
		
		if(j == 0 )TotnoOfAdults += RoomOnenoOfAdults;
		if(j == 1 )TotnoOfAdults += RoomTwonoOfAdults;
		if(j == 2 )TotnoOfAdults += RoomThreenoOfAdults;
		
		if(j == 0 )TotnoOfChildrens += RoomOnenoOfChildrens;
		if(j == 1 )TotnoOfChildrens += RoomTwonoOfChildrens;
		if(j == 2 )TotnoOfChildrens += RoomThreenoOfChildrens;
		
		
		
		}

		logger.info("checkInDate------------>"+checkInDate);
		logger.info("checkOutDate------------>"+checkOutDate);

		logger.info("--------------"+vendorid+" RIDE HotelCall-------------");

		
			//Creating the PopertyAvailability request
			AvailabilityRequest request = new AvailabilityRequest();
			
			request.setId(id);
			
			if (cacheKey != null) {
				request.setCacheKey(cacheKey);
				request.setSearchType(1);
				request.setRequestType(1);
				request.setMarkUpList(markups);
			} else{
				//If the Serach data is not in  the Cache tables taking from the suppliers
				logger.info("Local :No Cache Key - vendor- " + vendors[0][0]);
				
				//Set the request Data
				request.setCheckInDate(checkInDate);
				request.setCheckOutDate(checkOutDate);
				request.setNoOfAdults(TotnoOfAdults);
				request.setCurrentVendorOrder(1);
				request.setNoOfChildren(TotnoOfChildrens);
				request.setNoOfRooms(numberOfRooms);
				request.setCountry(countryCode); 
				request.setCity(cityCode);
				request.setSuburb(suburbcode);
				request.setState(State);
				request.setPortal(portal);
				request.setStatus("F");
				request.setMarkUpList(markups);
				request.setVendorList(vendors);
				request.setSortedBy(3);
				request.setRoomOccupants(roomOccupants);
				request.setStart(0);
				request.setNoOfResults(9999);
				request.setSessionId("99");
				request.setPartnerLevel(PartnerLevelType.B2C);
				request.setExchangeRateMethod(ExchangeRateMethodType.sellingRate);
				//request.setRequestCurrencyType(CurrencyCodeType.KWD);
				request.setLanguageCode("en");
				
				//request.setRequestMethod("JSON");
				//request.setRequestPackage("REZG");
				//request.setRequestPackage("RIDE");
				
				//request.setCombineRooms(true);
				//request.setCombineHotels(true);
				
				if(vendorid.equals("amadeus")){ 	
					
				request.setOmitRooms(true);					
				request.setFilterByHotel(false);
				
				HotelList selectedHotelList = new HotelList();
				selectedHotelList.setHotelCount(1);
				List<VendorHotel> vendorHotel = new ArrayList<VendorHotel>();
				VendorHotel VendorHotel = new VendorHotel();
				Hotel hotel = new Hotel();
				hotel.setRideCode("amadeus-AA5-IN");
				hotel.setHotelCode("AA5-IN");
				hotel.setVendorID("amadeus");;
				VendorHotel.addHotel(hotel );
				vendorHotel.add(VendorHotel );
				selectedHotelList.setVendorHotel(vendorHotel );
				
				//request.setSelectedHotelList(selectedHotelList );
				
				String [] s = {"amadeus-AA5-IN"};
				
				//request.setSelectedHotelRideCodeList(s);
				
				}else if(vendorid.equals("hbsi")){
					
					HotelList hotelList = new HotelList();
					
					VendorHotel vendorHotel = new VendorHotel();	
					
					Hotel hotelObj = new Hotel();
					hotelObj.setHotelCode("40418");
					
					Room roomObj = new Room();
					roomObj.setRoomType("DELUXE");
					
					RoomOccupant roomOccupant = new RoomOccupant();
					roomOccupant.setAdultCount(1);
					roomObj.setRoomOccupants(roomOccupant);
					
					
					Rate rate =  new Rate();
					rate.setRateCode("PREV");
					
					roomObj.addRate(rate);
					
					hotelObj.addRoom(roomObj);
					
					vendorHotel.addHotel(hotelObj);
					
					hotelList.addVendorHotel(vendorHotel);

					request.setSelectedHotelList(hotelList);
				
				}
				
				Properties prop = new Properties();
				prop.put("HotelCode","0");
				prop.put("HOTEL_CODE","0");
				prop.put("HOTEL_ATTRACTIONID","ALL");
				prop.put("HOTEL_LOCID","ALL");
				prop.put("BED_TYPE_ID_0","%" );
				prop.put("HOTEL_GROUPID","ALL");
				prop.put("LANGUAGE_CODE","en");
				prop.put("HOTEL_STARID","ALL");
				prop.put("NIGHTS","6");
				prop.put("ROOM_TYPE_ID_0","%");
				prop.put("RES_SOURCE","WEB");
				prop.put("USER_CODE","0");
				prop.put("CURRENCY","AUD");
				prop.put("HOTEL_TYPEID","ALL");
				prop.put("USER_TYPE","WEB");
				prop.put("TOUR_OPR_ID","0");
				prop.put("customercountry","LK");

				request.setVendorSpecificProperties(prop);
				
			}
			
			ride.services.connectorservices.ServiceData rideServiceData = new ride.services.connectorservices.ServiceData();
			
			rideServiceData.setServiceName("RIDEService");
			rideServiceData.setSchemaName(schema);
			rideServiceData.setModuleName("hotels");
			rideServiceData.setActionMethod("");
			rideServiceData.setInputData(request);


			logger.info("---------1---------");
			
			RIDEServer rideServer = new RIDEServer();
			rideServer.doService(rideServiceData);

			/*
			
			//logger.info("---------Calling Remote Server---------");
			//String host = "192.168.1.135";
			//String host = "www4.secure-reservation.com";
			String host = "www.secure-atlanticcity.com";
			//String host = "localhost";
			//String host = "mc5.rezgateway.com"; // 67.23.26.233
			
			System.setProperty("java.security.policy","/rezsystem/ride_rmi/client.policy");
			
			// Assign security manager
			if (System.getSecurityManager() == null)
			{
				System.setSecurityManager
				(new RMISecurityManager());
			}
			

			RemoteService service 		= (RemoteService) Naming.lookup("rmi://" + host + ":2010/RemoteService");
			rideServiceData 			= (ride.services.connectorservices.ServiceData)service.doService(rideServiceData);
			*/
			/*
			
			 Socket mySock = null;
			
			try{
			
			   mySock = new Socket(host,2020);
		         
		       mySock.setKeepAlive(true); 

		       
		       ObjectOutputStream ostr = new ObjectOutputStream(mySock.getOutputStream());
		       ostr.writeObject(rideServiceData);
		       ostr.flush();
		       
		       ObjectInputStream isr = new ObjectInputStream(mySock.getInputStream());
		       rideServiceData = (ride.services.connectorservices.ServiceData) isr.readObject();
		      
			}catch (Exception e) {
					// TODO: handle exception
				logger.error("--Error Calling Ride RMI Socket--", e);
			}finally{
					mySock.close();
			}
		       
			logger.info("---------Remote Server Over---------");
			
			*/

			logger.info("---------2-----------");
			
			
			try {
				
				response = (AvailabilityResponse) rideServiceData
						.getOutputData(0);

				logger.info("---------3-----------");
				
				Thread.sleep(10); // to give time to print results ;)
				
/*				Obj2Json		obj2Json   	= new Obj2Json();
				Json2Obj		json2Obj    = new Json2Obj();
				
				String 			json 		= obj2Json.AvailabilityResponse2Json(response);
				
				File 						mFile 							= new File("/home/sameera/Desktop/sample.json");
				
				mFile.createNewFile();
				FileOutputStream 			fos 							= new FileOutputStream(mFile,false);
				fos.write(json.getBytes());
				fos.close();
				
				response					= json2Obj.Json2AvailabilityResponse(json);*/
						
				logger.info("---------4----------");
				
				hotels = (HotelList) response.getHotelList();
				errors = (ArrayList) response.getErrors();
				
			} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
				List errors = new ArrayList(1);
				response.setErrors(errors);
			}
			
			
			
			logger.warn("------------Ride Server Called---------");

			logger.info("errors.size()--->"+errors.size());
			
			if (errors.size() > 0) {
				for (int i = 0; i < errors.size(); i++) {
					logger.info("Error Code - >"+((RIDEErrors)response.getErrors().get(i)).getErrorCode());
					logger.info("Error Desc - >"+((RIDEErrors)response.getErrors().get(i)).getError());
				}
			}
			
			
			//else{
				
		        
		        logger.info("CacheKey--------->"+response.getCacheKey());
		        logger.info("RequestID--------->"+response.getRequestID());
				
				cacheKey = response.getCacheKey();
				logger.info("Local :Cache Key " + cacheKey);
				try {
					
					HotelList hotels = response.getHotelList();
					
					int hotelCount = hotels.getHotelCount();
					

					logger.info("hotelCount----------> "+ hotelCount);
					

					ArrayList<Hotel> hotellist = new ArrayList<Hotel>();
					
					for (int i = 0; i < hotelCount; i++) {
						
						VendorHotel 		vHotel 				= (VendorHotel) hotels.getVendorHotel(i);
						
						String 				minimumVendor 		= vHotel.getMinimumVendor();						
						
						ArrayList<Hotel> 	samevendorhotels 	= vHotel.getVendorHotels(minimumVendor);
						
						Hotel				hotel2				= vHotel.getMinimumVendorHotel();	

						logger.info("MinimumVendorHotel----------> "+ hotel2);
		
						Hotel				hotel3				= vHotel.getMinimumOnRequestVendorHotel();
						
						if(null != hotel3)	logger.info("MinimumOnRequestVendorHotel----------> "+ hotel3);
						
						
						Hotel				hotel4				= vHotel.getMinimumAvailableVendorHotel();
						
						if(null != hotel4)	logger.info("MinimumAvailableVendorHotel----------> "+ hotel4);

						if(samevendorhotels.size() > 1) {
							
							logger.info("*******************************************************************************************");
							
							logger.info("--"+((Hotel)samevendorhotels.get(0)).getHotelName() +" is a Mulitiple Same verndor hotel !!--");
							
							logger.info("*******************************************************************************************");
						}

						logger.info("vHotel.getAvailableHotels()-->"+vHotel.getAvailableHotels().size());
						logger.info("vHotel.getOnRequestHotels()-->"+vHotel.getOnRequestHotels().size());
						
						for (Hotel hotel : vHotel.getAvailableHotels()) {
							
							logger.info("--"+ hotel +" is a Available Mulitiple Same verndor hotel !!--");
							
							//logger.info("hotel4-->" + hotel4.equals(hotel));
							
						}
						
						for (Hotel hotel : vHotel.getHotel()) {
							logger.info("--"+hotel.getRideCode()+" - "+hotel.getHotelCode()+" - "+hotel.getHotelName()+" - "+hotel.getVendorID()+"--");
						}
						
						hotellist.add(hotel2);
						
					}
										
					logger.info("SupplierResultStatus----------> "+ response.getSupplierResultStatus());
					logger.info("SupplierStatus---------> "+ response.getSupplierStatus());

					logger.info("hotellist size----------> "+ hotellist.size());
					
					for (int i = 0; i < hotellist.size(); i++) {

						Hotel mHotel = hotellist.get(i);
	
						if(i == 0) Test2Hotel = mHotel;
						
						String rcodes = "";
						
						for (int j = 0; j < mHotel.getRoom().size(); j++) {
							rcodes += "," + mHotel.getRoom().get(j).getRoomCode();
						}
						
						rcodes = rcodes.replaceFirst(",", "");
						
						logger.warn(i + " - "+mHotel.getVendorID()+" | "+mHotel.getHotelCode()+" | "+mHotel.getRideCode()+" | "+mHotel.getHotelName() +" | " + rcodes +" | " + mHotel.getRoom().size()+" | " + mHotel.getSuburb()+" | " + mHotel.getAmenitiesList() +" | " + mHotel.getStarCatagory());
						
						if(null == SelectedRoomCode){
							if(rcodes.indexOf(",") > 0) SelectedRoomCode = rcodes.substring(0, rcodes.indexOf(","));
							else						SelectedRoomCode = rcodes;
						}

						
/*						logger.info("HotelCode -------------->"+ i + " - "+mHotel.getHotelCode().trim());
						logger.info("RideCode -------------->"+ i + " - "+mHotel.getRideCode());
						logger.info("productid------>"+mHotel.getVendorHotelData("productid"));
						logger.info("Hotel Name -------------->"+ i + " - "+mHotel.getHotelName().trim());
						logger.info("Hotel City -------------->"+ i + " - "+mHotel.getCity());
						logger.info("Hotel Country -------------->"+ i + " - "+mHotel.getCountry());
						logger.info("Hotel AverageRate -------------->"+ i + " - "+mHotel.getAverageRate());
						logger.info("Hotel ThumbNailURL-->"+mHotel.getThumbNailURL());
						logger.info("Hotel VendorHotelData-->"+mHotel.getVendorHotelData());
						logger.info("Hotel ShortDescription------>"+mHotel.getShortDescription());*/
						
						List Roomslist = mHotel.getRoom();
						
						for(int r=0;r<Roomslist.size();r++){
							
							Room room = (Room)Roomslist.get(r);
							
							
/*							logger.info("Hotel room Name -------------->"+ r + " - "+room.getRoomName());
							logger.info("Hotel room Code -------------->"+ r + " - "+room.getRoomCode());
							logger.info("Hotel RoomNo -------------->"+ r + " - "+room.getRoomOccupants().getRoomNo());
							logger.info("Hotel room TotalRate -------------->"+ r + " - "+((Rate)room.getRates().get(0)).getTotalRate());
							logger.info("Hotel room TotalRateMinusMarkUp -------------->"+ r + " - "+((Rate)room.getRates().get(0)).getTotalRateMinusMarkUp());
							logger.info("Hotel room AverageRateMinusMarkUp -------------->"+ r + " - "+((Rate)room.getRates().get(0)).getAverageRateMinusMarkUp());
*/
							
							//logger.info("------------------------------------------------");
							
						}
						
						if( (null != testHotelCode && mHotel.getHotelCode().trim().equals(testHotelCode)) || (null != testHotelName && mHotel.getHotelName().trim().equals(testHotelName))){  
							Test2Hotel = mHotel;
							logger.info("------------------Making the Test Hotel----------------");
						} 
						
					}
				} catch (Exception err) {
					err.printStackTrace();
					logger.info("ERROR IN PRINTING HOTEL CODES :" + err.toString());
				}
				
				
				logger.info("------------------The Test Hotel Details----------------");
				
				if(null != Test2Hotel){
				
				logger.info("Hotel HotelName------>"+Test2Hotel.getHotelName());
				logger.info("Hotel VendorHotelData-->"+Test2Hotel.getVendorHotelData());
				logger.info("Hotel reqid-->"+Test2Hotel.getVendorHotelData("reqid"));
				logger.info("Hotel VendorName-->"+Test2Hotel.getVendorName());
				logger.info("Hotel City-->"+Test2Hotel.getCity());
				logger.info("Hotel Country-->"+Test2Hotel.getCountry());
				logger.info("Hotel Currency-->"+Test2Hotel.getCurrency());
				logger.info("Hotel Address------>"+Test2Hotel.getAddress());				
				logger.info("Hotel getShortDescription------>"+Test2Hotel.getShortDescription());				
				logger.info("Hotel AverageRate------>"+Test2Hotel.getAverageRate());
				logger.info("Hotel MinimumRate-->"+Test2Hotel.getMinimumRate());
				logger.info("Hotel MaximumRate-->"+Test2Hotel.getMaximumRate());
				logger.info("Hotel getRoom().size()-->"+Test2Hotel.getRoom().size());				
				logger.info("Hotel Policy-->"+Test2Hotel.getPolicy());
				logger.info("Hotel StarCatagory-->"+Test2Hotel.getStarCatagory());
				logger.info("Hotel ThumbNailURL-->"+Test2Hotel.getThumbNailURL());				
				logger.info("Hotel Imageurl-->"+Test2Hotel.getImageurl());	
				logger.info("Hotel OnRequest-->"+Test2Hotel.isOnRequest());				
				
				logger.info("Hotel ThumbNailURL2-->"+Test2Hotel.getVendorHotelData("imagethumbnailurl"));				
				logger.info("Hotel Imageurl2-->"+Test2Hotel.getVendorHotelData("imageurl"));				
				
				logger.info("Hotel PromotionType------>"+Test2Hotel.getPromotionType());
				
				List<Promotion> hpromos = Test2Hotel.getPromotions();
				
				if(null != hpromos){
					
					for (Promotion promotion : hpromos) {
						logger.info("hotel PromotionCode------>"+promotion.getPromotionCode());
						logger.info("hotel PromotionName------>"+promotion.getPromotionName());
						logger.info("hotel PromotionDescription------>"+promotion.getPromotionDescription());
					}
					
				}
				
				logger.info("Room Size---->"+Test2Hotel.getRoom().size());
				
				ArrayList newRoomslist = new ArrayList();
				
				logger.info("------------------The Test Hotel Room Details----------------");

				for(int r = 0;r<Test2Hotel.getRoom().size();r++){

					String Rcode = null;
					Room testroom = (Room)Test2Hotel.getRoom().get(r);
					
					logger.info("getRoomName------>"+testroom.getRoomName());
					logger.info("getRoomCode------>"+testroom.getRoomCode());
					logger.info("RoomNo------>"+testroom.getRoomOccupants().getRoomNo());
					logger.info("AdultCount------>"+testroom.getRoomOccupants().getAdultCount());
					logger.info("ChildCount------>"+testroom.getRoomOccupants().getChildCount());
					logger.info("ChildAges------>"+testroom.getRoomOccupants().getChildAges());
					Rcode = testroom.getRoomCode();
					logger.info("getBedType------>"+testroom.getBedType());
					logger.info("getBedCode------>"+testroom.getBedCode());	
					logger.info("MinimumRate------>"+testroom.getMinimumRate().getTotalRate());	
					logger.info("BestRate------>"+testroom.isBestRate());			
					logger.info("OnRequest------>"+testroom.isOnRequest());
					logger.info("VendorRoomData------>"+testroom.getVendorRoomData());	
					
/*					List<Promotion> promos = testroom.getPromotions();
					
					if(null != promos){
						
						for (Promotion promotion : promos) {
							logger.info("Room PromotionCode------>"+promotion.getPromotionCode());
							logger.info("Room PromotionName------>"+promotion.getPromotionName());
							logger.info("Room PromotionDescription------>"+promotion.getPromotionDescription());
						}
						
					}*/
					
					logger.info("Rate Size------>"+testroom.getRates().size());
					
					logger.info("------------------The Test Hotel Rate Details----------------");					

					for(int t = 0;t<testroom.getRates().size();t++){
						
					Rate testrate = (Rate)testroom.getRates().get(t);
					logger.info("RateCode------>"+testrate.getRateCode());
					logger.info("RateType------>"+testrate.getRateType());
					logger.info("getTotalRate------>"+testrate.getTotalRate());
					logger.info("getCurrency------>"+testrate.getCurrency());
					logger.info("getTotalRateMinusMarkUp------>"+testrate.getTotalRateMinusMarkUp());					
					logger.info("getAverageRate------>"+testrate.getAverageRate());
					logger.info("getVendorRateData------>"+testrate.getVendorRateData());
					
					logger.info("------------------The Test Hotel DailyRate Details----------------");
					for(int d = 0;d<testrate.getDailyRates().size();d++){
						
						DailyRate testdailyrate = (DailyRate)testrate.getDailyRates().get(d);
						
						logger.info("testdailyrate Day------>"+testdailyrate.getDay());
						logger.info("testdailyrate DailyTotal------>"+testdailyrate.getDailyTotal());
						logger.info("testdailyrate StandardRate------>"+testdailyrate.getStandardRate());
						logger.info("testdailyrate BeforeMarkupDailyRoomRate------>"+testdailyrate.getBeforeMarkupDailyRoomRate());
						logger.info("testdailyrate getVendorRateData------>"+testdailyrate.getVendorRateData());					
						
					}
					
					}

					
					
					if(SelectedRoomCode == null){
						newRoomslist.add(((Room)Test2Hotel.getRoom().get(r)));
					}else if(null!=Rcode && (SelectedRoomCode.indexOf(Rcode)>-1)){
						logger.info("---Adding ROmm---");
						
						Rate rate = ((Room)Test2Hotel.getRoom().get(r)).getRates().get(0);
						
						((Room)Test2Hotel.getRoom().get(r)).getRates().clear();
						
						((Room)Test2Hotel.getRoom().get(r)).getRates().add(rate);
						
						newRoomslist.add(((Room)Test2Hotel.getRoom().get(r)));
					}
					
					logger.info("-------------------------------------------------------------------");
					logger.info("");
				}
				
				
				
				
				logger.info("newRoomslist Size------>"+newRoomslist.size());
				
				Test2Hotel.setRoom(newRoomslist);

				for (int i = 0; i < Test2Hotel.getRoom().size(); i++) {
					
					Room room = (Room)Test2Hotel.getRoom().get(i);
					
					logger.info("Final RoomCode-->"+room.getRoomCode());
					
					for (int j = 0; j < room.getRates().size(); j++) {
						
						Rate rate = (Rate)room.getRates().get(j);
						
						logger.info("Final rate-->"+rate.getRateCode());
						
					}
								
					
				}
				
			}else{
				logger.warn("Test Hotel is null !!");
			}
				
				
				
			//}

			return response; 	
		}


	public void HotelRoomSearchCall() throws Exception {
		
		//Thread.sleep(1000*2);
	
		logger.info("--------------RIDE RoomCall-------------");
		portal         = schema+"_hotels";
		
		String HotelCode = "-";
		String CacheKey = "-";
		
		markups 	= new Object[1][2];
		vendors 	= new String[1][2];		
		
		markups[0][0] = vendorid;
		markups[0][1] = markup;
		
		vendors[0][0] =  vendorid;
		vendors[0][1] = "1";

//		Test2Hotel.getRoom().get(0).getRates().get(0).setVendorData("totalRate", "100.0");
		
		if(Test2Hotel==null){
			
			logger.info("Test2Hotel is null !!!!!!! making one !!!!!");
			
			Test2Hotel = new Hotel();
			Test2Hotel.setHotelCode(testHotelCode);
			Test2Hotel.setHotelName("Test Hotel Name with null Hotel");
			Room room = new Room();
			room.setRoomCode(SelectedRoomCode);
			
			roomOccupants = new RoomOccupant[1];
			roomOccupants[0] = new RoomOccupant();			
			roomOccupants[0].setRoomNo((1));	
			roomOccupants[0].setAdultCount(1);
			roomOccupants[0].setChildAges("7");
			roomOccupants[0].setChildCount(0);		
			room.setRoomOccupants(roomOccupants[0]);
			Test2Hotel.addRoom(room);
			
			Test2Hotel.setVendorHotelData("paxDomicile","LK");

			
		}
		
		try{HotelCode = Test2Hotel.getHotelCode();}catch (Exception e) {logger.info(e.toString());}
		try{CacheKey = response.getCacheKey();}catch (Exception e) {logger.info(e.toString());}
		
		/*		for(int h = 0;h < roomOccupants.length;h++){
		logger.info("roomOccupants[h].getRoomNo()-->"+roomOccupants[h].getRoomNo());
				}*/
		
		RoomAvailabilityRequest roomRequest = new RoomAvailabilityRequest();
		//rezg.dto.ride.common.roomavailability.RoomAvailabilityRequest roomRequest = new rezg.dto.ride.common.roomavailability.RoomAvailabilityRequest();
		
		roomRequest.setId(id);
		
		roomRequest.setCheckInDate(checkInDate);
		roomRequest.setCheckOutDate(checkOutDate);
		roomRequest.setNoOfAdults(TotnoOfAdults);
		roomRequest.setNoOfChildren(TotnoOfChildrens);
		roomRequest.setNoOfRooms(numberOfRooms);
		roomRequest.setCountry(countryCode); //France 214   // 70 GB
		roomRequest.setState(State);
		roomRequest.setCity(cityCode);//paris 1210  //2374 London

		roomRequest.setNearByLocations("RS001");
    	roomRequest.setPortal(portal);
		roomRequest.setRequestType(2);
		
		roomRequest.setSellingCurrency("USD");
		roomRequest.setSellingExchanerate(1.0);
		
		roomRequest.setRoomOccupants(roomOccupants);

		logger.info("HotelRoomSearchCall().Test2Hotel.getVendorCurrency() RQ : " + Test2Hotel.getVendorCurrency());
		
		roomRequest.setHotel(Test2Hotel);
		
		roomRequest.setHotelCode(HotelCode);
		roomRequest.setVendorList(new String[] { vendorid });
		roomRequest.setCacheKey(CacheKey);
		roomRequest.setMarkUpList(markups);
		
		ride.services.connectorservices.ServiceData roomServiceData = new ride.services.connectorservices.ServiceData();
		roomServiceData.setInputData(roomRequest);
		roomServiceData.setActionMethod("RIDEService");
		roomServiceData.setServiceName("RIDEService");
		roomServiceData.setActionMethod("");

		//System.out.println("RoomOccupants--------------->" + roomRequest.getHotel().getRoom().get(0).getRoomOccupants());
		
		RIDEServer server = new RIDEServer();
		server.doService(roomServiceData);
		
		
/*		logger.info("---------Calling Remote Server---------");
		
		//String host 			= "localhost";
		String host = "www4.secure-reservation.com";
		
		System.setProperty("java.security.policy","/rezsystem/ride_rmi/client.policy");
		
		// Assign security manager
		if (System.getSecurityManager() == null)
		{
			System.setSecurityManager
			(new RMISecurityManager());
		}
		
		RemoteService service 	= (RemoteService) Naming.lookup("rmi://" + host + ":2010/RemoteService");
		roomServiceData 		= (ride.services.connectorservices.ServiceData)service.doService(roomServiceData);
		
		logger.info("---------Remote Server Over---------");*/
		
				
		logger.info("Got Room Availability Response");
		RoomAvailabilityResponse roomResponse = (RoomAvailabilityResponse) roomServiceData.getOutputData(0);
		
		logger.info("roomResponse-->"+roomResponse);
		
		List<RideMessage>			rideMessages =  roomResponse.getRideMessages();
		
		for (RideMessage rideMessage : rideMessages) {
			logger.info("rideMessage-->"+rideMessage);
		}
		
		errors = (ArrayList) roomResponse.getErrors();		
	
		if (errors.size() > 0) {
		for (int i = 0; i < errors.size(); i++) {
		logger.info("Error-->"+((RIDEErrors) errors.get(i)).getError());
		}
		} else {
			
		roomAvailabilityHotel = roomResponse.getHotel();
		
		ArrayList rooms = (ArrayList) roomAvailabilityHotel.getRoom();
		
		
		logger.info("RoomAvail Hotel :Hotel--->"+roomAvailabilityHotel);
		logger.info("RoomAvail Hotel :Hotel Code-->"+roomAvailabilityHotel.getHotelCode());
		logger.info("RoomAvail Hotel :RideCode-->"+roomAvailabilityHotel.getRideCode());
		logger.info("RoomAvail Hotel :Hotel Name-->"+roomAvailabilityHotel.getHotelName());
		logger.info("RoomAvail Hotel :Hotel VendorHotelData-->"+roomAvailabilityHotel.getVendorHotelData());
		logger.info("RoomAvail Hotel :Hotel VendorID-->"+roomAvailabilityHotel.getVendorID());
		logger.info("RoomAvail Hotel :PromotionType------>"+Test2Hotel.getPromotionType());
		logger.info("RoomAvail Hotel :isOnRequest------>"+Test2Hotel.isOnRequest());
		
		logger.info("HotelRoomSearchCall().Test2Hotel.getVendorCurrency() RS : " + Test2Hotel.getVendorCurrency());
		
		List<Promotion> hpromos = roomAvailabilityHotel.getPromotions();
		
		if(null != hpromos){
			
			for (Promotion promotion : hpromos) {
				logger.info("RoomAvail: hotel PromotionCode------>"+promotion.getPromotionCode());
				logger.info("RoomAvail: hotel PromotionName------>"+promotion.getPromotionName());
				logger.info("RoomAvail: hotel PromotionDescription------>"+promotion.getPromotionDescription());
			}
			
		}
		
		logger.info("RoomAvail :Hotel Rooms--->"+rooms.size());
		
		for (int i = 0; i < rooms.size(); i++) {
		roomAvailabilityRoom = (Room) rooms.get(i);
		
		logger.info("=========================================================");
		
		logger.info("RoomAvail :Room Code-->"+ roomAvailabilityRoom.getRoomCode());
		logger.info("RoomAvail :Room Name-->"+ roomAvailabilityRoom.getRoomName());
		logger.info("RoomAvail :Room Description-->"+ roomAvailabilityRoom.getRoomDescription());		
		logger.info("RoomAvail :Room RoomNo - "+ roomAvailabilityRoom.getRoomOccupants().getRoomNo());
		logger.info("RoomAvail :Room AdultCount --> "+ roomAvailabilityRoom.getRoomOccupants().getAdultCount());
		logger.info("RoomAvail :Room BedType --> "+ roomAvailabilityRoom.getBedType());
		logger.info("RoomAvail :Room BedCode --> "+ roomAvailabilityRoom.getBedCode());
		logger.info("RoomAvail :Room OnRequest-->"+ roomAvailabilityRoom.isOnRequest());
		
		List<Promotion> promos = roomAvailabilityRoom.getPromotions();
		
		if(null != promos){
			
			for (Promotion promotion : promos) {
				logger.info("RoomAvail :Room PromotionCode------>"+promotion.getPromotionCode());
				logger.info("RoomAvail :Room PromotionName------>"+promotion.getPromotionName());
				logger.info("RoomAvail :Room PromotionDescription------>"+promotion.getPromotionDescription());
			}
			
		}
		
		try{
			
			
			
			for (int j = 0; j < roomAvailabilityRoom.getRates().size(); j++) {
				
				logger.info("RoomAvail :Rate Code-->" + ((Rate) roomAvailabilityRoom.getRates().get(j)).getRateCode());
				logger.info("RoomAvail :Rate Type-->" + ((Rate) roomAvailabilityRoom.getRates().get(j)).getRateType());
				logger.info("RoomAvail :Rate TotalRate-->" + ((Rate) roomAvailabilityRoom.getRates().get(j)).getTotalRate());
				logger.info("RoomAvail :Rate TotalRateMinusMarkUp-->" + ((Rate) roomAvailabilityRoom.getRates().get(j)).getTotalRateMinusMarkUp());
				logger.info("RoomAvail :Rate Currency-->" + ((Rate) roomAvailabilityRoom.getRates().get(j)).getCurrency());
				logger.info("RoomAvail :Rate VendorRateData-->" + ((Rate) roomAvailabilityRoom.getRates().get(j)).getVendorRateData());
				
				List DailyRates = ((Rate) roomAvailabilityRoom.getRates().get(j)).getDailyRates();
				
				for (Iterator iter = DailyRates.iterator(); iter.hasNext();) {
					DailyRate mDaily = (DailyRate) iter.next();
					logger.info("RoomAvail :DailyRate Day-->"+ mDaily.getDay());
					logger.info("RoomAvail :DailyRate DailyTotal-->"+ mDaily.getDailyTotal());
					logger.info("RoomAvail :DailyRate StandardRate-->"+ mDaily.getStandardRate());
					logger.info("RoomAvail :DailyRate BeforeMarkupDailyRoomRate-->"+ mDaily.getBeforeMarkupDailyRoomRate());
					logger.info("RoomAvail :DailyRate VendorRateData-->"+ mDaily.getVendorRateData());
				}
				
			}
		}catch (Exception e) {
			// TODO: handle exception
		}
		

		}

		logger.info("Room Avl cancellationCharge------->"+roomAvailabilityHotel.getVendorHotelData().getProperty("cancellationCharge"));
		logger.info("Room Avl NoOfUnits_BeforeArival------->"+roomAvailabilityHotel.getVendorHotelData().getProperty("NoOfUnits_BeforeArival"));
		logger.info("Room Avl cancelation_policy------->"+roomAvailabilityHotel.getVendorHotelData().getProperty("cancelation_policy"));
		logger.info("Room Avl  CANCEL_DEADLINE------->"+roomAvailabilityHotel.getVendorHotelData().getProperty("CANCEL_DEADLINE"));
		
		logger.info("Room Avl  roomAvailabilityHotel.getCancellationPolicy().size()------->"+roomAvailabilityHotel.getCancellationPolicy().size());
		
	  for (int j = 0; j < roomAvailabilityHotel.getCancellationPolicy().size(); j++) {
	   List cancelPolicyList=roomAvailabilityHotel.getCancellationPolicy();
	   CancellationPolicy cancelPolicy=(CancellationPolicy)cancelPolicyList.get(j);
	   logger.info("***********************************************");
	   logger.info("cancelPolicy.getDeadline():"+cancelPolicy.getDeadline());
	   logger.info("cancelPolicy.getIsFreeText():"+cancelPolicy.isFreeText());
	   logger.info("cancelPolicy.getPolicyText():"+cancelPolicy.getPolicyText());
	   logger.info("CancellationPolicyType:"+roomAvailabilityHotel.getCancellationPolicyType());
	   
	   if(null!=cancelPolicy.getCancellationPolicyCharge()){
		   logger.info("cancelPolicy.getCancellationPolicyCharge().size():"+cancelPolicy.getCancellationPolicyCharge().size());
		   for(int k = 0;cancelPolicy.getCancellationPolicyCharge().size()>k;k++){
			   CancellationPolicyCharge ccharge =  ((CancellationPolicyCharge)cancelPolicy.getCancellationPolicyCharge().get(k));
			   
			   logger.info("CancellationChargeFreeText-->"+ccharge.getCancellationChargeFreeText());
			   logger.info("FromDate-->"+ccharge.getFromDate());
			   logger.info("ToDate"+ccharge.getToDate());
			   logger.info("CheckInDatemarkup-->"+ccharge.getCheckInDatemarkup());
			   logger.info("XmlCancellationChargeRate-->"+ccharge.getXmlCancellationChargeRate());
			   logger.info("XmlCancellationChargeUsdRate-->"+ccharge.getXmlCancellationChargeUsdRate());
			   logger.info("CancellationChargeMarkupRate-->"+ccharge.getCancellationChargeMarkupRate());
			   logger.info("CancellationChargePortalRate-->"+ccharge.getCancellationChargePortalRate());
			   logger.info("getSellingCurrency-->"+ccharge.getSellingCurrency());
			   logger.info("getSellingExchanerate-->"+ccharge.getSellingExchanerate());
			   logger.info("getChargeType-->"+ccharge.getChargeType());
		   }
	   }
	   
	  }
		
		
		preReservationHotel = roomAvailabilityHotel;
		}
		logger.info("************** Finish Room Avaiability***********************");
			
	}

public void HotelPreReservationCall() throws Exception {
	
	logger.info("--------------RIDE PreReservationCall-------------");
	portal         = schema+"_hotels";


	logger.info("preReservationHotel.getRoom().size()-->"+preReservationHotel.getRoom().size());
	
		// looping for no of rooms
		ArrayList rooms = new ArrayList();
		for (int i = 0; i < numberOfRooms; i++) {
			preReservationRoom = (Room) preReservationHotel.getRoom().get(i);
			preReservationRoomOccupant =preReservationRoom.getRoomOccupants();
			preReservationRoomOccupant.setOccupantTittle("MR");
			preReservationRoomOccupant.setOccupantFirstName("Test");
			preReservationRoomOccupant.setOccupantLastName("Test");
			preReservationRoomOccupant.setContact(contact);
			preReservationRoom.setRoomOccupants(preReservationRoomOccupant);
			rooms.add(preReservationRoom);
		}
		preReservationHotel.setRoom(rooms);
		PreReservationRequest resRequest = new PreReservationRequest();
		
		resRequest.setId(id);
		
		for (int i = 0; i < markups.length; i++) {
			if (markups[i][0].equals(vendorid)) {
				preReservationHotel.setProffitMarkUp( (double[]) markups[i][1]);
			}
		}
		
		Contact[] cont = new Contact[1];
		Contact con = new Contact();
		con.setFirstName("TestBook");
		con.setLastName("TestBooking");
		con.setTitle("MR");
		cont[0] = con;
		//resRequest.setReservationContact(cont);
		rezervationNo = "REZ"+(int)(Math.random()*100000);
		String [] rezNumbers = {rezervationNo};
		resRequest.setHotel(preReservationHotel);
		resRequest.setCheckInDate(checkInDate);
		resRequest.setCheckOutDate(checkOutDate);
		//resRequest.setPreReservationNo(rezNumbers);
		resRequest.setNoOfAdults(TotnoOfAdults);
		resRequest.setNoOfChildren(TotnoOfChildrens);
		resRequest.setNoOfRooms(numberOfRooms);
		resRequest.setRequestType(3);
		resRequest.setVendor(vendorid);
		resRequest.setPortal(portal);
		resRequest.setCountry(countryCode); //France 214
		resRequest.setCity(cityCode);   //1210 paris
		resRequest.setNearByLocations("");
		//resRequest.setCacheKey(response.getCacheKey());
		resRequest.setHotel(preReservationHotel);
		
		ServiceData ReservationServiceData = new ServiceData();
		ReservationServiceData.setActionMethod("RIDEService");
		ReservationServiceData.setServiceName("RIDEService");
		ReservationServiceData.setInputData(resRequest);
		
		RIDEServer server = new RIDEServer();
		server.doService(ReservationServiceData);
		
		
		PreReservationResponse resp = (PreReservationResponse)ReservationServiceData.getOutputData(0);
		
		logger.info("errors.size()--->"+resp.getErrors().size());
		
		if (resp.getRideMessages().size() > 0) {
			logger.info("MessageCode Code - >"+(resp.getRideMessages().get(0)).getMessageCode());
			logger.info("Message Desc - >"+(resp.getRideMessages().get(0)).getMessageDescription());
		}
		
		if (resp.getErrors().size() > 0) {
			logger.info("Error Code - >"+((RIDEErrors)resp.getErrors().get(0)).getErrorCode());
			logger.info("Error Desc - >"+((RIDEErrors)resp.getErrors().get(0)).getError());
		} else {
			logger.info("----PRE Resavation OK----");
			
			preReservationHotel = resp.getHotel();
			
			ArrayList pre_res_rooms = (ArrayList) preReservationHotel.getRoom();
			
			
			logger.info("PRE Resavation :Hotel--->"+preReservationHotel);
			logger.info("PRE Resavation :Hotel Code-->"+preReservationHotel.getHotelCode());
			logger.info("PRE Resavation :Hotel Name-->"+preReservationHotel.getHotelName());
			logger.info("PRE Resavation :Hotel VendorHotelData-->"+preReservationHotel.getVendorHotelData());
			
			logger.info("PRE Resavation :Hotel Rooms--->"+pre_res_rooms.size());
			
			for (int i = 0; i < pre_res_rooms.size(); i++) {
				
			Room	pre_res_room = (Room) pre_res_rooms.get(i);
			
			logger.info("=========================================================");
			
			logger.info("PRE Resavation :Room Code-->"+ pre_res_room.getRoomCode());
			logger.info("PRE Resavation :Room Name-->"+ pre_res_room.getRoomName());
			logger.info("PRE Resavation :Room Description-->"+ pre_res_room.getRoomDescription());		
			logger.info("PRE Resavation :Room RoomNo - "+ pre_res_room.getRoomOccupants().getRoomNo());
			logger.info("PRE Resavation :Room AdultCount - "+ pre_res_room.getRoomOccupants().getAdultCount());
			logger.info("PRE Resavation :Room BedType --> "+ pre_res_room.getBedType());
			
			try{

				for (int j = 0; j < roomAvailabilityRoom.getRates().size(); j++) {
					
					logger.info("=========================================================");
					
					logger.info("PRE Resavation :Rate Code-->" + ((Rate) pre_res_room.getRates().get(j)).getRateCode());
					logger.info("PRE Resavation :Rate Type-->" + ((Rate) pre_res_room.getRates().get(j)).getRateType());
					logger.info("PRE Resavation :Rate TotalRate-->" + ((Rate) pre_res_room.getRates().get(j)).getTotalRate());
					logger.info("PRE Resavation :Rate TotalRateMinusMarkUp-->" + ((Rate) pre_res_room.getRates().get(j)).getTotalRateMinusMarkUp());
					logger.info("PRE Resavation :Rate Currency-->" + ((Rate) pre_res_room.getRates().get(j)).getCurrency());
					logger.info("PRE Resavation :Rate VendorRateData-->" + ((Rate) pre_res_room.getRates().get(j)).getVendorRateData());
					
					List DailyRates = ((Rate) pre_res_room.getRates().get(j)).getDailyRates();
					
					for (Iterator iter = DailyRates.iterator(); iter.hasNext();) {
						DailyRate mDaily = (DailyRate) iter.next();
						logger.info("PRE Resavation :DailyRate Day-->"+ mDaily.getDay());
						logger.info("PRE Resavation :DailyRate DailyTotal-->"+ mDaily.getDailyTotal());
						logger.info("PRE Resavation :DailyRate StandardRate-->"+ mDaily.getStandardRate());
						logger.info("PRE Resavation :DailyRate VendorRateData-->"+ mDaily.getVendorRateData());
					}
					
				}
			}catch (Exception e) {
				// TODO: handle exception
			}
			

			}

			logger.info("PRE Resavation cancellationCharge------->"+preReservationHotel.getVendorHotelData().getProperty("cancellationCharge"));
			logger.info("PRE Resavation NoOfUnits_BeforeArival------->"+preReservationHotel.getVendorHotelData().getProperty("NoOfUnits_BeforeArival"));
			logger.info("PRE Resavation cancelation_policy------->"+preReservationHotel.getVendorHotelData().getProperty("cancelation_policy"));
			logger.info("PRE Resavation  CANCEL_DEADLINE------->"+preReservationHotel.getVendorHotelData().getProperty("CANCEL_DEADLINE"));
			
			logger.info("PRE Resavation  roomAvailabilityHotel.getCancellationPolicy().size()------->"+preReservationHotel.getCancellationPolicy().size());
			
		  for (int j = 0; j < preReservationHotel.getCancellationPolicy().size(); j++) {
		   List cancelPolicyList=preReservationHotel.getCancellationPolicy();
		   CancellationPolicy cancelPolicy=(CancellationPolicy)cancelPolicyList.get(j);
		   logger.info("***********************************************");
		   logger.info("PRE Resavation cancelPolicy.getDeadline():"+cancelPolicy.getDeadline());
		   logger.info("PRE Resavation cancelPolicy.getIsFreeText():"+cancelPolicy.isFreeText());
		   logger.info("PRE Resavation cancelPolicy.getPolicyText():"+cancelPolicy.getPolicyText());
		   if(null!=cancelPolicy.getCancellationPolicyCharge()){
			   for(int k = 0;cancelPolicy.getCancellationPolicyCharge().size()>k;k++){
				   CancellationPolicyCharge ccharge =  ((CancellationPolicyCharge)cancelPolicy.getCancellationPolicyCharge().get(k));
				   
				   logger.info("PRE Resavation CancellationChargeFreeText-->"+ccharge.getCancellationChargeFreeText());
				   logger.info("PRE Resavation FromDate-->"+ccharge.getFromDate());
				   logger.info("PRE Resavation ToDate"+ccharge.getToDate());
				   logger.info("PRE Resavation CheckInDatemarkup-->"+ccharge.getCheckInDatemarkup());
				   logger.info("PRE Resavation XmlCancellationChargeRate-->"+ccharge.getXmlCancellationChargeRate());
				   logger.info("PRE Resavation XmlCancellationChargeUsdRate-->"+ccharge.getXmlCancellationChargeUsdRate());
				   logger.info("PRE Resavation CancellationChargeMarkupRate-->"+ccharge.getCancellationChargeMarkupRate());
				   logger.info("PRE Resavation CancellationChargePortalRate-->"+ccharge.getCancellationChargePortalRate());
			   }
		   }
		   
		  }

			
		}
		
		logger.info("****************** Finish PreReservationCall *****************************");


}


public void HotelReservationCall() throws Exception {
	
	logger.info("--------------RIDE ReservationCall-------------");
	
	if(ConnectionConfig.getInstance().getRideServerIp().equals("ride.secure-reservation.com")){
		System.out.print("Going to do a Hotel Reservation in Live Server, Proceed (y/n) ? ");
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String input = reader.readLine();
		if(input.equals("y")) logger.info("Continuing...");
		else{
			logger.info("Abort...");
			throw new Exception("User aborted the HotelSearch()");
		}	
		
	}
	
	
	portal         = schema+"_hotels";
	
//
//	logger.info("Pre cancellationCharge------->"+preReservationHotel.getVendorHotelData().getProperty("cancellationCharge"));
//	logger.info("Pre NoOfUnits_BeforeArival------->"+preReservationHotel.getVendorHotelData().getProperty("NoOfUnits_BeforeArival"));
//	logger.info("Pre cancelation_policy------->"+preReservationHotel.getVendorHotelData().getProperty("cancelation_policy"));
//	
		// looping for no of rooms
		ArrayList rooms = new ArrayList();
		for (int i = 0; i < numberOfRooms; i++) {
			preReservationRoom = (Room) preReservationHotel.getRoom().get(i);
			preReservationRoomOccupant = preReservationRoom.getRoomOccupants();
			//preReservationRoomOccupant.setAdultCount(roomOccupants[i].getAdultCount());
			
			String s1 = "";
			String s2 = "";
			String s3 = "";
			
			for (int k = 0; k < roomOccupants[i].getAdultCount(); k++) {
				s1 += "Ms,";
				s2 += "TestFirtName"+k+",";
				s3 += "TestLastName"+k+",";				
			}
			
			s1 = s1.substring(0, s1.length()-1);
			s2 = s2.substring(0, s2.length()-1);
			s3 = s3.substring(0, s3.length()-1);
			
			
			preReservationRoomOccupant.setOccupantTittle(s1);
			preReservationRoomOccupant.setOccupantFirstName(s2);
			preReservationRoomOccupant.setOccupantLastName(s3);
			
			String c1 = "";
			String c2 = "";
			String c3 = "";
			
			try{
			
			for (int k = 0; k < roomOccupants[i].getChildCount(); k++) {
				c1 += "Ms,";
				c2 += "TestChildFName"+k+",";
				c3 += "TestChildLName"+k+",";				
			}
			
			c1 = c1.substring(0, c1.length()-1);
			c2 = c2.substring(0, c2.length()-1);
			c3 = c3.substring(0, c3.length()-1);
			
			}catch (Exception e) {
				// TODO: handle exception
			}
			
			preReservationRoomOccupant.setOccupantChildTittle(c1);
			preReservationRoomOccupant.setOccupantChildFirstName(c2);
			preReservationRoomOccupant.setOccupantChildLastName(c3);
			
			preReservationRoomOccupant.setChildCount(roomOccupants[i].getChildCount());
			preReservationRoomOccupant.setChildAges(roomOccupants[i].getChildAges());
			preReservationRoomOccupant.setContact(contact);
			preReservationRoom.setRoomOccupants(preReservationRoomOccupant);
			rooms.add(preReservationRoom);
		}
		preReservationHotel.setRoom(rooms);
		
		ReservationRequest resRequest = new ReservationRequest();
		
		
		resRequest.setPaymentToken("19af01e4-9525-415f-8aeb-e85ce1572cc0"); // live
		resRequest.setPaymentServiceId("rezbase_v3_test");
		resRequest.setPaymentServicePass("r9e8z7b6a5s4e_v3");
		resRequest.setPaymentSecureSecret("0747958603679485730482769384760334094509");
		resRequest.setPaymentConnectUrl("http://payments-test.secure-reservation.com/SecurePayments/RezPayService");
		
		resRequest.setId(id);
		
		Contact[] cont = new Contact[1];
		Contact con = new Contact();
		con.setFirstName("Test");
		con.setLastName("Test");
		con.setTitle("Mr");	
		
		
		CreditCard creditCard     = new CreditCard();
		
		creditCard.setCardNo("6111111111111116");
		creditCard.setCardType("VI");
		creditCard.setCvsNo("123");
		creditCard.setFirst_name("Test");
		creditCard.setLast_name("Test");
		
		
		con.setCard(creditCard);
		
		
		Address[]  adresss 				= new Address[1];
		adresss[0] 						= new Address();
		adresss[0].setStreetAddressLine1("travelnow");
		con.setAdresss(adresss);
		
		con.setTelephoneNo("0773 418 099");
		con.setEmailAddress("sameera@rezgateway.com");
		
		Properties vendorSpecificProperties = new Properties();
		vendorSpecificProperties.setProperty("Remarks", "to do :remarks enter here by the portal");
		//vendorSpecificProperties.setProperty("taproNo", "123654");
		resRequest.setVendorSpecificProperties(vendorSpecificProperties);
		
		
		
		cont[0] = con;
		resRequest.setReservationContact(cont);
		
		System.out.println("ReservationContact-->"+ resRequest.getReservationContact()[0]);
		
		
		//rezervationNo = "REZ"+(int)(Math.random()*100000);
		String [] rezNumbers = {rezervationNo};
		
		resRequest.setPortalBeforeConfId(new Integer("123"));
		
		resRequest.setHotel(preReservationHotel);
		resRequest.setCheckInDate(checkInDate);
		resRequest.setCheckOutDate(checkOutDate);
		resRequest.setPreReservationNo(rezNumbers);
		resRequest.setNoOfAdults(TotnoOfAdults);
		resRequest.setNoOfChildren(TotnoOfChildrens);
		resRequest.setNoOfRooms(numberOfRooms);
		resRequest.setRequestType(4);
		resRequest.setVendor(vendorid);
		resRequest.setPortal(portal);
		resRequest.setCountry(countryCode); //France 214
		resRequest.setCity(cityCode);   //1210 paris
		resRequest.setNearByLocations("");
		//resRequest.setCacheKey(response.getCacheKey());
		
		SimpleDateFormat 						sdf2									= new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"); 
		
		resRequest.setArrivalFlightDateTime(sdf2.format(checkInDate));
		resRequest.setDepatureFlightDateTime(sdf2.format(checkOutDate));
		resRequest.setFlightDetailsNeed(true);
		resRequest.setArrivalFlightNo("UL-500");
		resRequest.setDepatureFlightNo("UL-500");
		
		
		resRequest.setHotel(preReservationHotel);
		
		
		logger.info("req preReservationHotel getVendorHotelData-->"+roomAvailabilityHotel.getVendorHotelData());
		
		ServiceData ReservationServiceData = new ServiceData();
		ReservationServiceData.setActionMethod("RIDEService");
		ReservationServiceData.setServiceName("RIDEService");
		ReservationServiceData.setInputData(resRequest);
		
		RIDEServer server = new RIDEServer();
		server.doService(ReservationServiceData);		
		
		resp = (ReservationResponse) ReservationServiceData.getOutputData(0);		
		
		if (resp.getRideMessages().size() > 0) {
			logger.info("MessageCode Code - >"+(resp.getRideMessages().get(0)).getMessageCode());
			logger.info("Message Desc - >"+(resp.getRideMessages().get(0)).getMessageDescription());
		}
		
		logger.info("errors.size()--->"+resp.getErrors().size());
		
		if (resp.getErrors().size() > 0) {
			logger.info("Error Code - >"+((RIDEErrors)resp.getErrors().get(0)).getErrorCode());
			logger.info("Error Desc - >"+((RIDEErrors)resp.getErrors().get(0)).getError());
		} else {
			iteno 	= resp.getItenaryId();
			resNum  = resp.getReservatioNo();
			try {

				for (int i = 0; i < iteno.length; i++) {
					logger.info("Iternary #= "+iteno[i]);
					logger.info("Booking Rez#= "+resNum[i]);
					logger.info("BOOKING_RESULT = "+(resp.getVendorResponseProps()).getProperty("BOOKING_RESULT"));
					logger.info("supplierReferenceMessage = "+(resp.getVendorResponseProps()).getProperty("SUPPLIER_REFERENCE_MESSAGE"));
					logger.info("taproNo = "+(resp.getVendorResponseProps()).getProperty("taproNo"));
				}
				
				logger.info("PhoneNumber-->"+ resp.getHotelContactPhoneNumber());
				logger.info("HotelFax-->"+ resp.getHotelContactFaxNumber());
				logger.info("ContactEmail-->"+ resp.getHotelContactEmail());
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			logger.info("Rse cancellationCharge------->"+preReservationHotel.getVendorHotelData().getProperty("cancellationCharge"));
			logger.info("Rse NoOfUnits_BeforeArival------->"+preReservationHotel.getVendorHotelData().getProperty("NoOfUnits_BeforeArival"));
			logger.info("Rse cancelation_policy------->"+preReservationHotel.getVendorHotelData().getProperty("cancelation_policy"));
			
		}
		
		logger.info("props = "+(resp.getVendorResponseProps()));
		
		logger.info("****************** Finish Reservation *****************************");


		
		

}


public void HotelCancelationCall() throws Exception {
	
	logger.info("****************** Reservation Cancelation****************** ");
	portal         = schema+"_hotels";
	
	//
	
	//iteno[1]="111";
	//iteno[2]="222";
	
	Properties pro = null;
	
	if(null != resp){
		
		pro = resp.getVendorResponseProps();
		
	}else{

		iteno 				= new String[1];
		iteno[0]="9139B0014732";	
//		iteno[1]="11113IC008988";	
		
	pro = new Properties();
	
	pro.setProperty("ChainCode","9139");
	pro.setProperty("HotelCode",testHotelCode);
	pro.setProperty("AgentCode","SDI002");
	pro.setProperty("traceId","f7ab399d-6c90-4949-b148-1b38a68253e8");
	
	}
	
	if((iteno!=null) && (iteno.length > 0)){
		
	CancellationRequest _cancellationRequest = new CancellationRequest();
	
	_cancellationRequest.setId(id);
	
	_cancellationRequest.setCheckInDate(checkInDate);
	_cancellationRequest.setCheckOutDate(checkOutDate);
	_cancellationRequest.setNoOfRooms(numberOfRooms);
	
	_cancellationRequest.setRequestType(5);
	_cancellationRequest.setVendor(vendorid);
	_cancellationRequest.setPortal(portal);
	_cancellationRequest.setIternaryNo(iteno);
	_cancellationRequest.setVendorSpecificProperties(pro);
	
	ServiceData CancellationServiceData = new ServiceData();
	CancellationServiceData.setActionMethod("RIDEService");
	CancellationServiceData.setServiceName("RIDEService");
	CancellationServiceData.setInputData(_cancellationRequest);
	
	RIDEServer server = new RIDEServer();
	server.doService(CancellationServiceData);
	
	//logger.info("---------Calling Remote Server---------");
	
/*	String host = "www4.secure-reservation.com";
	//String host = "localhost";
	

	
	 Socket mySock = null;
	
	try{
	
	   mySock = new Socket(host,2020);
         
       mySock.setKeepAlive(true); 

       
       ObjectOutputStream ostr = new ObjectOutputStream(mySock.getOutputStream());
       ostr.writeObject(CancellationServiceData);
       ostr.flush();
       
       ObjectInputStream isr = new ObjectInputStream(mySock.getInputStream());
       CancellationServiceData = (ride.services.connectorservices.ServiceData) isr.readObject();
      
	}catch (Exception e) {
			// TODO: handle exception
		logger.error("--Error Calling Ride RMI Socket--", e);
	}finally{
			mySock.close();
	}
       */
	
	
	logger.info("---------Remote Server Over---------");
	
	
	
	
	CancellationResponse Cancelresp = (CancellationResponse)CancellationServiceData.getOutputData(0);
	
	if (Cancelresp.getErrors().size() > 0) {

	logger.info("Error Code - >"+((RIDEErrors)Cancelresp.getErrors().get(0)).getErrorCode());
	logger.info("Error Desc - >"+((RIDEErrors)Cancelresp.getErrors().get(0)).getError());
	
	} else {
	String  []cancelId = Cancelresp.getCancellationId();
	String  []cancelMsg = Cancelresp.getCancellationMessages();
	String  [] cancelationChage = Cancelresp.getCancellationAmmounts();
	
	logger.info("Iternary ID  = "+Cancelresp.getItenaryId());
	
	for (int i = 0; i < cancelId.length; i++) {
	logger.info("Cancelation ID = "+cancelId[i]);	
	
	}
	
	for (int i = 0; i < cancelMsg.length; i++) {
		logger.info("cancelMsg --> "+cancelMsg[i]);	
		
		}
	
		
	
	}
	}
	}

public void HotelContentDummy() throws RezException {
	
	logger.warn("HotelContentDummy().Start=========================================================");
	
	HotelContentRequest hotelContentRequest = new HotelContentRequest();
	hotelContentRequest.setRequestHotelID(testHotelCode);
	hotelContentRequest.setVendorId(vendorid);
	
	Properties vendorSpecificProperties = new Properties();
	vendorSpecificProperties.setProperty("AgentCode", "");
	hotelContentRequest.setVendorSpecificProperties(vendorSpecificProperties);
	
	
	ride.services.connectorservices.ServiceData serviceData = new ride.services.connectorservices.ServiceData();
	serviceData.setActionMethod("GetHotelContentData");
	serviceData.setSchemaName(schema);
	serviceData.setServiceName("RIDEService");
	serviceData.setModuleName("data");
	serviceData.setInputData(hotelContentRequest);
	
	RIDEServer server = new RIDEServer();
	server.doService(serviceData);
	
	logger.info("----------------Got HotelContent Response-----------------"+serviceData.getOutputData(0));
	
	HotelContentDTO HotelContentResponse = (HotelContentDTO) serviceData.getOutputData(0);
	
    logger.info("HotelDescription -> "+HotelContentResponse.getHotelDescription());
    logger.info("HotelName -> "+HotelContentResponse.getHotelName());
    logger.info("HotelImages -> "+HotelContentResponse.getHotelImages());
    logger.info("HotelAddressLine1 -> "+HotelContentResponse.getHotelAddressLine1());
    logger.info("HotelAddressLine2 -> "+HotelContentResponse.getHotelAddressLine2());
    logger.info("HotelEmail -> "+HotelContentResponse.getHotelEmail());
    logger.info("StarCategory -> "+HotelContentResponse.getHotelStarCategory());
    
    logger.info("Longitude -> "+HotelContentResponse.getLongitude());
    logger.info("Latitude -> "+HotelContentResponse.getLatitude());
    
    logger.info("--HotelFacilities - ");
    
    for (int i = 0; i < HotelContentResponse.getHotelFacilities().size(); i++) {
		
    	logger.info( HotelContentResponse.getHotelFacilities().get(i) + " --> " + HotelContentResponse.getHotelFacilitiesAvailability().get(i));
    	
	}
    
    logger.info("HotelAdditionalInfo -> "+HotelContentResponse.getHotelAdditionalInfo());
    logger.info("HotelAdditionalInfoType -> "+HotelContentResponse.getHotelAdditionalInfoType());
    
    logger.info("Amenity -> "+HotelContentResponse.getHotelTypeOfHotel());
    
	logger.info("************** Finish HotelContent***********************");
}

	
public void HotelContent() throws Exception {
	
	logger.info("--------------RIDE HotelContent Call-------------");
	
	if(Test2Hotel==null){
		
		logger.info("Test2Hotel is null !!!!!!! making one !!!!!");
		Test2Hotel = new Hotel();
		Test2Hotel.setHotelCode(testHotelCode);
		Test2Hotel.setHotelName("Test2Hotel");		
		
		Test2Hotel.setVendorHotelData("rezgAdminCity", cityCode);
		Test2Hotel.setVendorHotelData("traceId", "123456");
		Test2Hotel.setVendorHotelData("AgentCode", "SDI001");
		Test2Hotel.setVendorHotelData("DailyPassword", "03X77H38T66C82G13R90");
		
	}
	
	
	portal         = schema+"_hotels";
	HotelContentRequest ContentRequest = new HotelContentRequest();
	

	
	ContentRequest.setId(id);
	
	//ContentRequest.setHotelObj(Test2Hotel);
	ContentRequest.setVendorSpecificProperties(Test2Hotel.getVendorHotelData());
	ContentRequest.setVendorCityCode(cityCode);   //paris 1210  //2374 London
	ContentRequest.setRequestHotelID(Test2Hotel.getHotelCode());
	ContentRequest.setPortal(portal);
	ContentRequest.setRequestType(6);
	ContentRequest.setVendorHotelCode(Test2Hotel.getHotelCode());
	ContentRequest.setVendorId(vendorid);
	ContentRequest.setCacheKey(cacheKey);
	ContentRequest.setRequestType(6);
	ride.services.connectorservices.ServiceData ContentServiceData = new ride.services.connectorservices.ServiceData();
	ContentServiceData.setInputData(ContentRequest);
	ContentServiceData.setActionMethod("RIDEService");
	ContentServiceData.setServiceName("RIDEService");
	ContentServiceData.setActionMethod("");
	ContentServiceData.setInputData(ContentRequest);
	RIDEServer server = new RIDEServer();
	server.doService(ContentServiceData);
	
	logger.info("----------------Got HotelContent Response-----------------"+ContentServiceData.getOutputData(0));
	
	HotelContentDTO HotelContentResponse = (HotelContentDTO) ContentServiceData.getOutputData(0);

    logger.info("HotelDescription -> "+HotelContentResponse.getHotelDescription());
    logger.info("HotelName -> "+HotelContentResponse.getHotelName());
    logger.info("HotelImages -> "+HotelContentResponse.getHotelImages());
    logger.info("HotelAddressLine1 -> "+HotelContentResponse.getHotelAddressLine1());
    logger.info("HotelAddressLine2 -> "+HotelContentResponse.getHotelAddressLine2());
    logger.info("HotelEmail -> "+HotelContentResponse.getHotelEmail());
    logger.info("StarCategory -> "+HotelContentResponse.getHotelStarCategory());
    
    logger.info("Longitude -> "+HotelContentResponse.getLongitude());
    logger.info("Latitude -> "+HotelContentResponse.getLatitude());
    
    logger.info("--HotelFacilities - ");
    
    for (int i = 0; i < HotelContentResponse.getHotelFacilities().size(); i++) {
		
    	logger.info( HotelContentResponse.getHotelFacilities().get(i) + " --> " + HotelContentResponse.getHotelFacilitiesAvailability().get(i));
    	
	}
    
    logger.info("HotelAdditionalInfo -> "+HotelContentResponse.getHotelAdditionalInfo());
    logger.info("HotelAdditionalInfoType -> "+HotelContentResponse.getHotelAdditionalInfoType());
    
    logger.info("Amenity -> "+HotelContentResponse.getHotelTypeOfHotel());
    
	logger.info("************** Finish HotelContent***********************");
		
}

//public void CarSearchCall() throws Exception {
//	
//	logger.info("--------------RIDE Car SEARCH Call-------------");
//	
//	portal         = schema;
//	
//	name = "Amadeus";
//
//	Properties markups = new Properties();	
//	markups.setProperty(vendorid,"5");
//	
//	ArrayList vendors = new ArrayList();
//	vendors.add(vendorid);
//
//	rezg.ride.car.server.services.RIDEServer
//	
//	rezg.ride.car.server.availability.AvailabilityRequest availabilityRequest = new rezg.ride.car.server.availability.AvailabilityRequest();
//	availabilityRequest.setId(id);
//	availabilityRequest.setName(name);
//	//availabilityRequest.setNameList(nameList);			
//	availabilityRequest.setPickUpDateTime(pickUpDateTime);
//	availabilityRequest.setReturnDateTime(returnDateTime);
//	availabilityRequest.setPickUpLocationCode(pickUpLocationCode);
//	availabilityRequest.setReturnLocationCode(returnLocationCode);		
////	availabilityRequest.addvehicleCategory("ALL");
////	availabilityRequest.addvehicleCategory("3");
////	availabilityRequest.addvehicleCategory("CCAR");
////	availabilityRequest.addvehicleCategory("ALL");
////	availabilityRequest.addvehicleCategory("CSAR");
//	availabilityRequest.setPortal(portal);
//	availabilityRequest.setMarkups(markups);
//	availabilityRequest.setVendorList(vendors);
//	availabilityRequest.setCustomerCountryCode("UK");
//	
//	Properties prop = new Properties();
//	prop.put("CodeContext","GALILEO");
//	prop.put("driverAge","30");
//	availabilityRequest.setVendorSpecificProperties(prop);
//
//
//	ServiceData serviceData = new ServiceData();
//	serviceData.setServiceName("RIDEService");
//	serviceData.setActionMethod(""); 
//	serviceData.setInputData(availabilityRequest);
//	rezg.ride.car.server.services.RIDEServer server = new rezg.ride.car.server.services.RIDEServer();
//	server.doService(serviceData);
//	rezg.ride.car.server.availability.AvailabilityResponse availabilityResponse = (rezg.ride.car.server.availability.AvailabilityResponse) serviceData.getOutputData(0);
//
//	List<RIDEErrors> errorList = availabilityResponse.getErrors();
//	logger.info("Error Size >>"+errorList.size());
//
//	if(errorList.size() > 0){
//	for(int d = 0; d < errorList.size(); d++){
//		RIDEErrors errorObj = (RIDEErrors) errorList.get(d);
//		logger.info("--------------------Errors--------------------------");
//		logger.info("Error Location->" +errorObj.getErrorLocation());
//		logger.info("Error Type->" +errorObj.getErrorType());
//		logger.info("Error Code->" +errorObj.getErrorCode());
//		logger.info("Error->" +errorObj.getError());
//	}
//	
//}
//	
//	VendorResponse Obj = new VendorResponse();
//	
//	
//	for (int xx = 0; xx < availabilityResponse.getResponseList().size(); xx++) {
//		
//		Obj = (VendorResponse) availabilityResponse.getResponseList().get(xx); 
//		if(xx == 0){
//			vendorProps = Obj.getVendorSpecificData();
//		}
//		logger.info("PickUpDateTime - "+Obj.getPickUpDateTime());
//		logger.info("ReturnDateTime - "+Obj.getReturnDateTime());
//		logger.info("PickUpLocation - "+Obj.getPickUpLocation());
//		logger.info("PickUpLocationCode - "+Obj.getPickUpLocationCode());
//		logger.info("ReturnLocationCode - "+Obj.getReturnLocationCode());
//		logger.info("ReturnLocation - "+Obj.getReturnLocation());
//		
//		ArrayList vehicleList = Obj.getVehicle();
//		logger.info(vehicleList.size());
//		
//		for(int i = 0; i < vehicleList.size(); i++){						
//			logger.info("-----------------------------------");
//			logger.info("VendorCodeVehicle - " +(i+1));	
//
//			Vehicle vehicleObj = (Vehicle) vehicleList.get(i);
//			
//			resVehicleObj = vehicleObj;
//			
//			logger.info("AirCondition - "+vehicleObj.getAirCondition());
//			logger.info("TransmissionType - "+vehicleObj.getTransmissionType());
//			logger.info("VehicleCategory - "+vehicleObj.getVehicleCategory());
//			logger.info("VehicleClass - "+vehicleObj.getVehicleClass());
//			logger.info("VendorCarType - "+vehicleObj.getVendorCarType());
//			
//			Vendor venObj = vehicleObj.getVendorObj();
//			logger.info("Vendor Code - "+venObj.getVendorCode());
//			logger.info("Vendor Name - "+venObj.getVendor());
//			logger.info("Vendor Context - "+venObj.getVendorContext());
//			
//			Rental rentalObj = vehicleObj.getRentalObj();
//			logger.info("DistUnitName - "+rentalObj.getDistUnitName());
//			logger.info("DistUnlimited - "+rentalObj.getDistUnlimited());
//			logger.info("PeriodUnitName - "+rentalObj.getPeriodUnitName());
//			logger.info("RateQualifier - "+rentalObj.getRateQualifier());
//			logger.info("vendorCurrencyInfo- "+vehicleObj.getRentalObj().getVendorSpecificData().getProperty("vendorCurrencyInfo"));
//			logger.info("TotalAmount - "+rentalObj.getTotalAmount());									
//			
//			logger.info("vehicleObj.getImageUrl() - "+vehicleObj.getImageUrl());
//			
//			logger.info("vehicleObj.getVehicleCategory() - "+vehicleObj.getVehicleCategory());
//			
//			
//	
//			
//			
//			logger.info("getRefundableBranchDepositAmount() XML- "+rentalObj.getRefundableBranchDepositAmount());
//			
//			
//			ArrayList chargeList = rentalObj.getChargeList();
//			logger.info("Chrage Length --> "+chargeList.size());
//			for(int v = 0; v < chargeList.size(); v++){
//				logger.info("----------------- Charge ------------------");
//				Charge chargeObj = (Charge) chargeList.get(v);
//				logger.info("ChargeAmount - "+chargeObj.getChargeAmount());
//				logger.info("CurrencyCode - "+chargeObj.getCurrencyCode());
//				logger.info("GuaranteedInd - "+chargeObj.getGuaranteedInd());
//				logger.info("Purpose - "+chargeObj.getPurpose());
//				logger.info("TaxInclusive - "+chargeObj.getTaxInclusive());
//				logger.info("Description - "+chargeObj.getDescription());
//				logger.info("IncludedInRate - "+chargeObj.getIncludedInRate());
//			}
//			
//			/*Charge chargeObj = rentalObj.getCharge();
//			 logger.info("ChargeAmount - "+chargeObj.getChargeAmount());
//			 logger.info("CurrencyCode - "+chargeObj.getCurrencyCode());
//			 logger.info("GuaranteedInd - "+chargeObj.getGuaranteedInd());
//			 logger.info("Purpose - "+chargeObj.getPurpose());
//			 logger.info("TaxInclusive - "+chargeObj.getTaxInclusive()); */
//		}
//	}
//
//
//}

public void CarContentCall() throws Exception {
	
	portal         = schema;
	
	ContentRequest contentRequest  = new ContentRequest();
	contentRequest.setVendor(vendorid);
	contentRequest.setPortal(portal);
	contentRequest.setName(name);
	
	Properties vendorPropsFromContentRequest   = new Properties() ;// setting vendor properties from car content request only. 				
	vendorPropsFromContentRequest.put("PickupLocationCode", "LHR");
	vendorPropsFromContentRequest.put("ReturnLocationCode", "LHR");
	
	
	contentRequest.setVendorSpecificProperties(vendorProps);// vendor properties from availability. 
	contentRequest.setVehicleObj(resVehicleObj);
	contentRequest.setPickUpDateTime(pickUpDateTime);
	contentRequest.setReturnDateTime(returnDateTime);
	contentRequest.setLanguageCode("ES");
	ContentResponse  contentResponse = null;
	ServiceData serviceData = new ServiceData();
	serviceData.setServiceName("RIDEService");
	serviceData.setActionMethod(""); 
	serviceData.setInputData(contentRequest);
	
	rezg.ride.car.server.services.RIDEServer server = new rezg.ride.car.server.services.RIDEServer();
	server.doService(serviceData);
	
	contentResponse = (ContentResponse) serviceData.getOutputData(0);
	ArrayList errorList = contentResponse.getErrors();
	
	System.out.println("Error list length - "+errorList.size());
	if(errorList.size() > 0){
		
		for(int d=0; d<errorList.size(); d++){
			RIDEErrors errorObj = (RIDEErrors) errorList.get(d);
			logger.info("--------------------Errors--------------------------");
			logger.info("Error Location->" +errorObj.getErrorLocation());
			logger.info("Error Type->" +errorObj.getErrorType());
			logger.info("Error Code->" +errorObj.getErrorCode());
			logger.info("Error->" +errorObj.getError());
		}
		
	}else{
		
		logger.info("***************Content Response Resive **************************");

		/*
		ArrayList summary = contentResponse.getSummary();
		ArrayList hedings = contentResponse.getHedings();
		ArrayList retalCoditions = contentResponse.getRetalCoditions();
		
		try{
			logger.info("------------Summary--------------");
			logger.info("Summary Numbers = " + summary.size());
			for (int a = 0; a < summary.size(); a++) {
				logger.info(summary.get(a));
			}
			logger.info("");
		}catch(Exception e){
			logger.info("Error @ reading Summary");
		}
		
		try{
			logger.info("------------Hedings & Contents--------------");
			for (int a = 0; a < hedings.size(); a++) {
				logger.info("	"+hedings.get(a));
				try{
				ArrayList info = (ArrayList)retalCoditions.get(a);
				for (int b = 0; b < info.size(); b++) {
					logger.info(info.get(b));
				}
				logger.info("");
				}catch(Exception e){
					logger.info("Error @ reading Contents");
				}
				
			}
			logger.info("");
		}catch(Exception e){
			logger.info("Error @ reading Hedings & Contents");
		}

		*/
	}
}

public void CarPreReservationCall() throws Exception {
	
		logger.info("***************** calling car prereservation ******************");
		
		portal         = schema;
		
		Traveler travlerObj = new Traveler();
		travlerObj.setBirthDate("1984-07-24");
		travlerObj.setPassengerTypeCode("ADT");
		travlerObj.setNamePrefix("Mr");
		travlerObj.setGivenName("Ruchira");
		travlerObj.setSurname("Kariyawasam"); 
		travlerObj.setNameTitle("Mr");
		travlerObj.setTravelerRPH(1);
		
		
		
		CustomerInfo custInfoObj = new CustomerInfo();		
		
		custInfoObj.setCustEmail("ruchira@rezgateway.com");
		custInfoObj.setAddressType("Home");
		custInfoObj.setStreetNmbr("41/1 Dutugamunu Street");
		custInfoObj.setCityName("Colombo");
		custInfoObj.setAddressPostalCode(0);
		custInfoObj.setStateCode("");
		custInfoObj.setCountryCode("LK");
		
		
		CustTelephoneInfo custtelephoneInfoObj1 = new CustTelephoneInfo();
		custtelephoneInfoObj1.setPhoneLocationType("Home");	
		custtelephoneInfoObj1.setCountryAccessCode("1");
		custtelephoneInfoObj1.setAreaCityCode("MN");
		custtelephoneInfoObj1.setTelephoneNumber("445454545");
		
		CustTelephoneInfo custtelephoneInfoObj2 = new CustTelephoneInfo();
		custtelephoneInfoObj2.setPhoneLocationType("Business");	
		custtelephoneInfoObj2.setCountryAccessCode("0");
		custtelephoneInfoObj2.setAreaCityCode("MIA");
	//	custtelephoneInfoObj2.setTelephoneNumber("305-670-1561");
		custtelephoneInfoObj2.setTelephoneNumber("445454545");
		
		custInfoObj.addCustTeleList(custtelephoneInfoObj1);
		custInfoObj.addCustTeleList(custtelephoneInfoObj2);
		
		IteneraryInfo   itenerayInfoObj = new IteneraryInfo();
		itenerayInfoObj.setTicketTimeLimit("2013-12-17T12:00:00");
		itenerayInfoObj.setTicketType("eTicket");
		
		rezg.ride.car.server.datatypes.CreditCard ccInfoObj = new rezg.ride.car.server.datatypes.CreditCard();
		ccInfoObj.setCardType("VI");
		ccInfoObj.setCvsNo("123");			
		ccInfoObj.setExpieryDate(new Date(2013,8,30));
		ccInfoObj.setHolderName("Ruchira kariyawasam");
		ccInfoObj.setCardNo("4111111111111111");
		
		String Status  = "Available";
		int CarDataNumCars =1;
		int CarRate =4558;
		String CarRateCurrency ="USD";
		
		rezg.ride.car.server.prereservation.PreReservationRequest reservationRequest  = new rezg.ride.car.server.prereservation.PreReservationRequest();
		reservationRequest.setVendor(vendorid);
		reservationRequest.setPortal(portal);
		reservationRequest.setName(name);
		reservationRequest.setStatus(Status);
		reservationRequest.setPickUpDateTime(pickUpDateTime);
		reservationRequest.setReturnDateTime(returnDateTime);
		//reservationRequest
		//reservationRequest.setPicUPLocationCode(pickUpLocationCode);
		reservationRequest.setReturnLocationCode(returnLocationCode);
		reservationRequest.setVehicleObj(resVehicleObj);
//		reservationRequest.setCarDataNumCars(CarDataNumCars);
//		reservationRequest.setCarRate(CarRate);
		reservationRequest.setCarRateCurrency(CarRateCurrency);
		reservationRequest.setTravlerObj(travlerObj);
		reservationRequest.setCustInfoObj(custInfoObj);
		reservationRequest.setItenerayInfoObj(itenerayInfoObj) ;
		reservationRequest.setCcObj(ccInfoObj) ;
//		reservationRequest.addTelephonearrayList(custtelephoneInfoObj1);
		reservationRequest.setVendorSpecificProperties(vendorProps);			
//		reservationRequest.setAccountingLine("0123456789012345");			
		
//		reservationRequest.setTransferOwnershipRequired(false);
//		reservationRequest.setNewPseudoCityCode("123456");
		
		
		rezg.ride.car.server.prereservation.PreReservationResponse preReservationResponse = null;
		ServiceData serviceData = new ServiceData();
		serviceData.setServiceName("RIDEService");
		serviceData.setActionMethod(""); 
		serviceData.setInputData(reservationRequest);
		
		
		rezg.ride.car.server.services.RIDEServer server = new rezg.ride.car.server.services.RIDEServer();
		server.doService(serviceData);
		
		preReservationResponse = (rezg.ride.car.server.prereservation.PreReservationResponse) serviceData.getOutputData(0);
		List<rezg.ride.car.server.datatypes.RIDEErrors> errorList = preReservationResponse.getErrors();
		
		System.out.println("Prereservation Error list length - "+errorList.size());
		
		if(errorList.size() > 0){
			
			for(int d=0; d<errorList.size(); d++){
				rezg.ride.car.server.datatypes.RIDEErrors errorObj = (rezg.ride.car.server.datatypes.RIDEErrors) errorList.get(d);
				System.out.println("--------------------Errors--------------------------");
				System.out.println("Error Location->" +errorObj.getErrorLocation());
				System.out.println("Error Type->" +errorObj.getErrorType());
				System.out.println("Error Code->" +errorObj.getErrorCode());
				System.out.println("Error->" +errorObj.getError());
			}
			
		}
}

public void CarReservationCall() throws Exception {
	
	portal         = schema;
	
	Traveler travlerObj = new Traveler();
	travlerObj.setBirthDate("1984-07-24");
	travlerObj.setPassengerTypeCode("ADT");
	travlerObj.setNamePrefix("Mr");
	travlerObj.setGivenName("Ruchira");
	travlerObj.setSurname("Kariyawasam"); 
	travlerObj.setNameTitle("Mr");
	travlerObj.setTravelerRPH(1);
	
	CustomerInfo custInfoObj = new CustomerInfo();
	custInfoObj.setCustEmail("ruchira@rezgateway.com");
	custInfoObj.setAddressType("Home");
	custInfoObj.setStreetNmbr("41/1 Dutugamunu Street");
	custInfoObj.setCityName("Colombo");
	custInfoObj.setAddressPostalCode(0);
	custInfoObj.setStateCode("");
	custInfoObj.setCountryCode("LK");				
	
	CustTelephoneInfo custtelephoneInfoObj1 = new CustTelephoneInfo();
	custtelephoneInfoObj1.setPhoneLocationType("Home");	
	custtelephoneInfoObj1.setCountryAccessCode("1");
	custtelephoneInfoObj1.setAreaCityCode("MN");
	custtelephoneInfoObj1.setTelephoneNumber("445454545");
	
	CustTelephoneInfo custtelephoneInfoObj2 = new CustTelephoneInfo();
	custtelephoneInfoObj2.setPhoneLocationType("Business");	
	custtelephoneInfoObj2.setCountryAccessCode("0");
	custtelephoneInfoObj2.setAreaCityCode("MIA");
//	custtelephoneInfoObj2.setTelephoneNumber("305-670-1561");
	custtelephoneInfoObj2.setTelephoneNumber("445454545");
	
	custInfoObj.addCustTeleList(custtelephoneInfoObj1);
	custInfoObj.addCustTeleList(custtelephoneInfoObj2);
	
	IteneraryInfo   itenerayInfoObj = new IteneraryInfo();
	itenerayInfoObj.setTicketTimeLimit("2013-12-17T12:00:00");
	itenerayInfoObj.setTicketType("eTicket");
	
	rezg.ride.car.server.datatypes.CreditCard ccInfoObj = new rezg.ride.car.server.datatypes.CreditCard();
	ccInfoObj.setCardType("VI");
	ccInfoObj.setCvsNo("123");			
	ccInfoObj.setExpieryDate(new Date(2013,8,30));
	ccInfoObj.setHolderName("Ruchira kariyawasam");
	ccInfoObj.setCardNo("4111111111111111");
	
	String Status  = "Available";
	int CarDataNumCars =1;
	int CarRate =4558;
	String CarRateCurrency ="USD";
	
	rezg.ride.car.server.reservation.ReservationRequest reservationRequest  = new rezg.ride.car.server.reservation.ReservationRequest();
	reservationRequest.setVendor(vendorid);
	reservationRequest.setPortal(portal);
	reservationRequest.setName(name);
	reservationRequest.setStatus(Status);
	reservationRequest.setPickUpDateTime(pickUpDateTime);
	reservationRequest.setReturnDateTime(returnDateTime);
	reservationRequest.setPicUPLocationCode(pickUpLocationCode);
	reservationRequest.setReturnLocationCode(returnLocationCode);
	reservationRequest.setVehicleObj(resVehicleObj);
	reservationRequest.setCarDataNumCars(CarDataNumCars);
	reservationRequest.setCarRate(CarRate);
	reservationRequest.setCarRateCurrency(CarRateCurrency);
	reservationRequest.setTravlerObj(travlerObj);
	reservationRequest.setCustInfoObj(custInfoObj);
	reservationRequest.setItenerayInfoObj(itenerayInfoObj) ;
	reservationRequest.setCcObj(ccInfoObj) ;
	reservationRequest.addTelephonearrayList(custtelephoneInfoObj1);
	reservationRequest.setVendorSpecificProperties(vendorProps);			
	reservationRequest.setAccountingLine("0123456789012345");			
	
	reservationRequest.setTransferOwnershipRequired(false);
	reservationRequest.setNewPseudoCityCode("123456");
	
	
	
	ServiceData serviceData = new ServiceData();
	serviceData.setServiceName("RIDEService");
	serviceData.setActionMethod(""); 
	serviceData.setInputData(reservationRequest);
	
	
	rezg.ride.car.server.services.RIDEServer server = new rezg.ride.car.server.services.RIDEServer();
	server.doService(serviceData);
	
	rezg.ride.car.server.reservation.ReservationResponse reservationResponse = (rezg.ride.car.server.reservation.ReservationResponse) serviceData.getOutputData(0);
	ArrayList errorList = reservationResponse.getErrors();
	
	System.out.println("Error list length - "+errorList.size());
	
	if(errorList.size() > 0){
		
		for(int d=0; d<errorList.size(); d++){
			rezg.ride.car.server.datatypes.RIDEErrors errorObj = (rezg.ride.car.server.datatypes.RIDEErrors) errorList.get(d);
			System.out.println("--------------------Errors--------------------------");
			System.out.println("Error Location->" +errorObj.getErrorLocation());
			System.out.println("Error Type->" +errorObj.getErrorType());
			System.out.println("Error Code->" +errorObj.getErrorCode());
			System.out.println("Error->" +errorObj.getError());
		}
		
	}else{
//		This method use to check the Car Conformation Response Object
		System.out.println("***************Confirmed **************************");
		System.out.println("Version->" + reservationResponse.getVersion());
		System.out.println("Sucess->" + reservationResponse.isSucess());
		
		TravelItenerary traavelIteneryObj = reservationResponse.getTravelItenaryObj(); 
		System.out.println("ItineraryRef -Type->" + traavelIteneryObj.getIteneraryType());
		System.out.println("ItineraryRef -ID->" + traavelIteneryObj.getIteneraryId());
		
		ArrayList custInfoArrayList = traavelIteneryObj.getCustomerList();
		int custInfoArrayListSize = custInfoArrayList.size();
		
		for(int i=0; i < custInfoArrayListSize; i++){
			custInfoObj =(CustomerInfo)custInfoArrayList.get(i);
			System.out.println("CustomerInfo -RPH->" +custInfoObj.getCustRPH());
			System.out.println("personName -NameType->"+custInfoObj.getNameType());
			System.out.println("Person -GivenName->"+custInfoObj.getCustGivenName());
			System.out.println("Person -Surname->"+custInfoObj.getCustSurName());
			
			
			ArrayList telephoneList = custInfoObj.getTelephoneList();
			int telephoneListSize = telephoneList.size();
			for(int j=0; j < telephoneListSize; j++ ){
				CustTelephoneInfo custtelephoneInfoObj =(CustTelephoneInfo)telephoneList.get(j);
				System.out.println("Telephone --PhoneNumber->" +custtelephoneInfoObj.getTelephoneNumber());
				System.out.println("Telephone --PhoneUseType->" +custtelephoneInfoObj.getTelephoneUseType());		
				System.out.println("-------------------------------------------------");
			}
			
			System.out.println("Email->"+custInfoObj.getCustEmail());
		}
		
	}
	
}

public void CarCancelationCall() throws Exception {
	
	portal         = schema;
	
	System.out.println("--------Caliiing car cancellation-----");
		
		String bookingNo="6UFCND";
		
		//String bookingNo = reservationResponse.getConfID() ;
	
	/*
	 * this is for cartrawler
	 */
	if(null == vendorProps)  vendorProps = new Properties();	
	vendorProps.setProperty("GivenName","JOHN");
	vendorProps.setProperty("Surname","TEST");
	vendorProps.setProperty("NameSuffix","R");
	vendorProps.setProperty("NamePrefix","Mr");
	vendorProps.setProperty("PickUpDateTime","2011-01-23T09:00:00Z");
	vendorProps.setProperty("ReturnDateTime","2011-01-27T09:00:00Z");
	vendorProps.setProperty("PickUpCodeContext","CARTRAWLER");
	vendorProps.setProperty("PickUpLocationCode","4513");
	vendorProps.setProperty("ReturnCodeContext","CARTRAWLER");
	vendorProps.setProperty("ReturnLocationCode","4513");
	vendorProps.setProperty("ReturnLocationCode","4513");
	vendorProps.setProperty("citizenCountrycode","US");
	
	vendorProps.setProperty("RequestID","46784_1330671314574");
	vendorProps.setProperty("SessionID","test");				
	
	rezg.ride.car.server.cancellation.CancellationRequest CancelRequest  = new rezg.ride.car.server.cancellation.CancellationRequest();
	CancelRequest.setVendor(vendorid);
	CancelRequest.setPortal(portal);
	CancelRequest.setName(name);
	CancelRequest.setBookingNo(bookingNo);
	
	//CancelRequest.setVendorSpecificProperties(vendorProps); // vendorprops from the cancellation only. 
	//CancelRequest.setVendorSpecificProperties(reservationResponse.getVendorSpecificProperties()); // vendorprops from the reservation. 
	
	rezg.ride.car.server.cancellation.CancellationResponse  cancelResponse = null;
	ServiceData serviceData = new ServiceData();
	serviceData.setServiceName("RIDEService");
	serviceData.setActionMethod(""); 
	serviceData.setInputData(CancelRequest);
	
	rezg.ride.car.server.services.RIDEServer server = new rezg.ride.car.server.services.RIDEServer();
	server.doService(serviceData);
	
	cancelResponse = (rezg.ride.car.server.cancellation.CancellationResponse) serviceData.getOutputData(0);
	
	logger.info("cancelResponse>>>>"+ cancelResponse);
	
}

public void TransferAvailabilityCall() throws Exception {

	
	/*
	rideattraction.availability.AvailabilityResponse availabilityResponse 	= null;
	rideattraction.availability.AvailabilityRequest availabilityRequest 	= new rideattraction.availability.AvailabilityRequest();
	
	System.out.println("************Availability Request****************");
	
	availabilityRequest.setStartDate(checkInDate);
	availabilityRequest.setEndDate(checkOutDate);
	availabilityRequest.setNoOfAdults(noOfAdult);
	availabilityRequest.setNoOfChildren(noOfChild);
	availabilityRequest.setChildAges(childAges);
	availabilityRequest.setAdultAges(adultAges);
	availabilityRequest.setCity(city);
	availabilityRequest.setState("-");
	availabilityRequest.setCountry(country);
	availabilityRequest.setNearByLocations("");
	availabilityRequest.setPortal(portal);
	availabilityRequest.setMarkUpList(markups);
	availabilityRequest.setVendorList(vendors);
	availabilityRequest.setRequestType(1);
	availabilityRequest.setSortedBy(1);
	availabilityRequest.setStart(0);
	availabilityRequest.setNoOfResults(10);
	availabilityRequest.addProgramTypes("SIC");
	availabilityRequest.setPickUpCode(pickUpType);
	availabilityRequest.setDropOffCode(dropOffType);
	availabilityRequest.setPickUpCityCode(city);
	availabilityRequest.setTransferTime(time1);
	
	
	Properties prop = new Properties();
	prop.put("customercountry","GB");
	availabilityRequest.setVendorSpecificProperties(prop);
	
	System.out.println("XXX - 1 - ");
	ServiceData serviceData = new ServiceData();
	System.out.println("XXX - 2 - ");
	serviceData.setServiceName("RIDEActivityService");
	System.out.println("XXX - 3 - ");
	serviceData.setActionMethod("");
	System.out.println("XXX - 4 - ");
	serviceData.setInputData(availabilityRequest);
	System.out.println("XXX - 5 - ");
	
	//RIDEServer server = new RIDEServer(callerType);
	RIDEServer server = new RIDEServer();
	System.out.println("XXX - 6 - ");
	server.doService(serviceData);
	System.out.println("XXX - 7 - ");
	//System.exit(0);
	availabilityResponse = (AvailabilityResponse) serviceData.getOutputData(0);
	errors = (ArrayList) availabilityResponse.getErrors();
	System.out.println("Errors.size() = " + errors.size());
	
	if (errors.size() > 0) {
		for (int i = 0; i < errors.size(); i++) {
			RIDEErrors error = (RIDEErrors)errors.get(i);
			System.out.println("ErrorCode :: " + error.getErrorCode()+ "	Error :: " + error.getError());
		}
	} else {			
		
		ProgramList programs = availabilityResponse.getProgramList();
		int programCount = programs.getProgramCount();
		
		System.out.println("Local ::Available ProgramImpl Count - "+ programCount);
		boolean printDetails = true;
//		boolean print = true;
		
		for (int i = 0; i < programCount; i++) {
			
			VendorProgram mVendorProgram = (VendorProgram) programs.getVendorProgram(i);
			String minimumVendor = mVendorProgram.getMinimumVendor();
			System.out.println("minimumVendor:="+minimumVendor);
			Program mProgram = (Program) mVendorProgram.getVendorProgram(minimumVendor);
			
			System.out.println("aaaaaaaa:="+((Activity)(mProgram.getActivity().get(0))).getActivityCode());
//			System.out.println("bbbbbbbb AvailableModalityCode:="+((Activity)(mProgram.getActivity().get(0))).getVendorActivityData("AvailableModalityCode"));
			
//			if (mProgram.getVendorProgramData().getProperty("TicketInfoCode").equals("FLAMENCOPA") && ((Activity)(mProgram.getActivity().get(0))).getVendorActivityData("AvailableModalityCode").equals("0#1SHOWCOPA")) {
			
			if (i == selectedProgramindex) {
				this.selectedProgramObj = mProgram;
				this.vendorProperties = this.selectedProgramObj.getVendorProgramData();
			}
			
			if (printDetails) {
				System.out.println("MinimumVendor >> "+minimumVendor);
				
				System.out.println("============= Program "+(i+1)+" ===============");
				System.out.println("ProgramName ::" + mProgram.getProgramName());
				System.out.println("ProgramCode ::" + mProgram.getProgramCode());
				System.out.println("AverageRate ::" + mProgram.getAverageRate());
				System.out.println("VendorID ::" + mProgram.getVendorID());
				System.out.println("RideCode ::" + mProgram.getRideCode());
				System.out.println("ProgramCatagory ::" + mProgram.getProgramCatagory());
				System.out.println("Country ::" + mProgram.getCountry());
				System.out.println("City ::" + mProgram.getCity());
				System.out.println("State ::" + mProgram.getState());
				
				for (int aa = 0; aa < mProgram.getActivity().size(); aa++) {
					System.out.println("-------------- Activity "+(aa+1)+" -------------------");
					Activity mActivity = ((Activity)mProgram.getActivity().get(aa));
					System.out.println("ActivityCode ::"+mActivity.getActivityCode());
					System.out.println("ActivityName ::"+mActivity.getActivityName());
					
					for (int ww = 0; ww < mActivity.getDailyInv().size(); ww++) {
						DailyInv mDailyInv = (DailyInv) (mActivity.getDailyInv().get(ww));
						System.out.println("DailyDate->"+mDailyInv.getDailyDate());
						System.out.println("DailyInv->"+mDailyInv.getDailyInv());
					}
					
					for (int bb = 0; bb < ((Activity)mProgram.getActivity().get(aa)).getPeriod().size(); bb++) {
						System.out.println("-------------- Period "+(bb+1)+" ----------------");
						Period mPeriod = ((Period)((Activity)mProgram.getActivity().get(aa)).getPeriod().get(bb));
						System.out.println("PeriodId ::"+mPeriod.getPeriodId());
						System.out.println("StartDate ::"+mPeriod.getStartDate());
						System.out.println("StartTime ::"+mPeriod.getStartTime());
						
						Properties periodProperties = mPeriod.getVendorPeriodData();
						for (Iterator iter = periodProperties.keySet().iterator(); iter.hasNext();) {
							String key = (String) iter.next();
							System.out.println(key + " >> " + periodProperties.getProperty(key));
						}
					}
					
					for (int xx = 0; xx < ((Activity)mProgram.getActivity().get(aa)).getRates().size(); xx++) {
						System.out.println("-------------Rate "+ (xx+1) +"-------------------------");
						Rate mRate = ((Rate)((Activity)mProgram.getActivity().get(aa)).getRates().get(xx));
						System.out.println("RateCode ::"+mRate.getRateCode());
						System.out.println("RateType ::"+mRate.getRateType());
						System.out.println("StayDay ::"+mRate.getStayDay());
						System.out.println("Netrate ::"+mRate.getNetrate());
						System.out.println("Sellrate ::"+mRate.getSellrate());
						System.out.println("Currency ::"+mRate.getCurrency());
						System.out.println("TotalRate ::"+mRate.getTotalRate());
						System.out.println("TotalRateMinusMarkUp ::"+mRate.getTotalRateMinusMarkUp());
					}
				}
			}else{
				System.out.println("============= Program "+(i+1)+" ===============");
				System.out.println("ProgramName ::" + mProgram.getProgramName());
				System.out.println("ProgramCode ::" + mProgram.getProgramCode());
				System.out.println("AverageRate ::" + mProgram.getAverageRate());
				System.out.println("VendorID ::" + mProgram.getVendorID());
				System.out.println("RideCode ::" + mProgram.getRideCode());
				System.out.println("ProgramCatagory ::" + mProgram.getProgramCatagory());
				System.out.println("Country ::" + mProgram.getCountry());
				System.out.println("City ::" + mProgram.getCity());
				System.out.println("State ::" + mProgram.getState());
			}
		}

	}
*/
	
}

public void TransferProgramAvailabilityCall() throws Exception {
	

	/*

	
	System.out.println("************Program Availability Request****************");			
	ProgramAvailabilityRequest programAvailabilityRequest 	= new ProgramAvailabilityRequest();
	programAvailabilityRequest.setCheckInDate(new Date(checkInDate));
	programAvailabilityRequest.setCheckOutDate(new Date(checkOutDate));
	programAvailabilityRequest.setNoOfAdults(noOfAdult);
	programAvailabilityRequest.setNoOfChildren(noOfChild);
	programAvailabilityRequest.setProgram(this.selectedProgramObj);
	
	programAvailabilityRequest.setChildAges(childAges);
	programAvailabilityRequest.setAdultAges(adultAges);
	programAvailabilityRequest.setCity(city);
	programAvailabilityRequest.setState("-");
	programAvailabilityRequest.setCountry(country);
	programAvailabilityRequest.setNearByLocations("");
	programAvailabilityRequest.setPortal(portal);
	programAvailabilityRequest.setMarkUpList(markups);
	programAvailabilityRequest.setVendorList(new String[]{vendorId});
	programAvailabilityRequest.setVendorSpecificProperties(this.vendorProperties);
	programAvailabilityRequest.setRequestType(2);
//	System.out.println("hotelListMap XXX--->"+selectedProgramObj.getVendorProgramData("hotelListMap"));
//	System.out.println("hotelListMap XXX--->"+((HashMap<String, String>)selectedProgramObj.getVendorProgramData("hotelListMap")).size());
	
	ServiceData serviceData = new ServiceData();
	serviceData.setServiceName("RIDEActivityService");
	serviceData.setActionMethod("");
	
	serviceData.setInputData(programAvailabilityRequest);
	
	
	System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMM [111]: programAvailabilityRequest.setMarkUpList(markups) : "+programAvailabilityRequest.getMarkUpList());
	
	RIDEServer server = new RIDEServer();
	server.doService(serviceData);
	
	ProgramAvailabilityResponse programAvailabilityResponse = (ProgramAvailabilityResponse) serviceData.getOutputData(0);
	
	errors = (ArrayList) programAvailabilityResponse.getErrors();
	System.out.println("Errors >> " + errors.size());
	
	if (errors.size() > 0) {
		for (int i = 0; i < errors.size(); i++) {
			RIDEErrors error = (RIDEErrors)errors.get(i);
			System.out.println("ErrorCode :: " + error.getErrorCode()+ "	Error :: " + error.getError());
		}
	} else {			
		
		programResProgram= programAvailabilityResponse.getProgram();
		
		System.out.println("============= Program ===============");
		//System.out.println("ProgramName ::" + programResProgram.getProgramName());
		System.out.println("ProgramCode ::" + programResProgram.getProgramCode());
		System.out.println("AverageRate ::" + programResProgram.getAverageRate());
		System.out.println("VendorID ::" + programResProgram.getVendorID());
		System.out.println("RideCode ::" + programResProgram.getRideCode());
		System.out.println("ProgramCatagory ::" + programResProgram.getProgramCatagory());
		
		for (int aa = 0; aa < programResProgram.getActivity().size(); aa++) {
			System.out.println("-------------- Activity "+(aa+1)+" -------------------");
			Activity mActivity = ((Activity)programResProgram.getActivity().get(aa));
			System.out.println("ActivityCode ::"+mActivity.getActivityCode());
			System.out.println("ActivityName ::"+mActivity.getActivityName());
			
			for (int ww = 0; ww < mActivity.getDailyInv().size(); ww++) {
				DailyInv mDailyInv = (DailyInv) (mActivity.getDailyInv().get(ww));
				System.out.println("DailyDate ::"+mDailyInv.getDailyDate());
				System.out.println("DailyInv  ::"+mDailyInv.getDailyInv());
			}
			
			
			for (int bb = 0; bb < ((Activity)programResProgram.getActivity().get(aa)).getPeriod().size(); bb++) {
				System.out.println("-------------- Period "+(bb+1)+" ----------------");
				Period mPeriod = ((Period)((Activity)programResProgram.getActivity().get(aa)).getPeriod().get(bb));
				System.out.println("PeriodId  ::"+mPeriod.getPeriodId());
				System.out.println("StartDate ::"+mPeriod.getStartDate());
				System.out.println("StartTime ::"+mPeriod.getStartTime());
				
				System.out.println("----Period Vendor Props-------");
				Properties periodProperties = mPeriod.getVendorPeriodData();
				for (Iterator iter = periodProperties.keySet().iterator(); iter.hasNext();) {
					String key = (String) iter.next();
					System.out.println(key + " >> " + periodProperties.getProperty(key));
				}
			}
			for (int xx = 0; xx < ((Activity)programResProgram.getActivity().get(aa)).getRates().size(); xx++) {
				System.out.println("-------------Rate "+ (xx+1) +"-------------------------");
				Rate mRate = ((Rate)((Activity)programResProgram.getActivity().get(aa)).getRates().get(xx));
				System.out.println("RateCode ::"+mRate.getRateCode());
				System.out.println("RateType ::"+mRate.getRateType());
				System.out.println("Netrate  ::"+mRate.getNetrate());
				System.out.println("Sellrate ::"+mRate.getSellrate());
				System.out.println("Currency ::"+mRate.getCurrency());
				System.out.println("TotalRate::"+mRate.getTotalRate());
				System.out.println("TotalRateMinusMarkUp ::"+mRate.getTotalRateMinusMarkUp());
				
				System.out.println("----Rate Vendor Props-------");
				for (Iterator iter = mRate.getVendorRateData().keySet().iterator(); iter.hasNext();) {
					String key = (String) iter.next();
					System.out.println(key + " ::: " + mRate.getVendorRateData().getProperty(key));
				}
			}
			System.out.println("----Activity Vendor Props-------");
			for (Iterator iter = mActivity.getVendorActivityData().keySet().iterator(); iter.hasNext();) {
				String key = (String) iter.next();
				System.out.println(key + " ::: " + mActivity.getVendorActivityData().getProperty(key));
			}
		}
		System.out.println("----Program Vendor Props-------");
		//System.out.println("hotelListMap XXX-2-->"+programResProgram.getVendorProgramData("hotelListMap"));
		//System.out.println("hotelListMap XXX-2-->"+((HashMap<String, String>)programResProgram.getVendorProgramData("hotelListMap")).size());
		
		for (Iterator iter = programResProgram.getVendorProgramData().keySet().iterator(); iter.hasNext();) {
			String key = (String) iter.next();
			System.out.println(key + " ::: " + programResProgram.getVendorProgramData().get(key));
		}
		
	}

*/

}

public void TransferProgramContentCall() throws Exception {

	/*
	System.out.println("************Program content Request****************");			
	ProgramContentRequest programContentRequest 	= new ProgramContentRequest();
//	programContentRequest.setCheckInDate(new Date(checkInDate));
//	programContentRequest.setCheckOutDate(new Date(checkOutDate));
//	programContentRequest.setNoOfAdults(noOfAdult);
//	programContentRequest.setNoOfChildren(noOfChild);
//	programContentRequest.setChildAges(childAges);
//	programContentRequest.setCity(city);
//	programContentRequest.setState("-");
//	programContentRequest.setCountry(country);
//	programContentRequest.setNearByLocations("");
	programContentRequest.setProgram(this.selectedProgramObj);
	programContentRequest.setVendorProgramCode(selectedProgramObj.getProgramCode());
	System.out.println("Selected Program Code : "+selectedProgramObj.getProgramCode());		
	
	
	programContentRequest.setVendorCityCode(city);
	programContentRequest.setPortal(portal);
	
	
	programContentRequest.setVendorId(vendorId);
	programContentRequest.setVendorSpecificProperties(this.vendorProperties);
	programContentRequest.setRequestType(5);
	
	ServiceData serviceData = new ServiceData();
	serviceData.setServiceName("RIDEActivityService");
	serviceData.setActionMethod("");
	
	serviceData.setInputData(programContentRequest);
	
	RIDEServer server = new RIDEServer();
	server.doService(serviceData);
	
	ProgramContentResponse programContentResponse =  (ProgramContentResponse) serviceData.getOutputData(0);
	
	errors = (ArrayList) programContentResponse.getErrors();
	System.out.println("Errors >> " + errors.size());
	
	if (errors.size() > 0) {
		for (int i = 0; i < errors.size(); i++) {
			RIDEErrors error = (RIDEErrors)errors.get(i);
			System.out.println("ErrorCode :: " + error.getErrorCode()+ "	Error :: " + error.getError());
		}
	} else {			
		
		//Program mProgram = programContentResponse.getProgram();
		
		System.out.println("TestRIDEServer1 assertTest programContentResponse.getAdditionalInfo()---->"+programContentResponse.getAdditionalInfo());;
		

		
	}
*/

}

public void TransferReservationCall() throws Exception {

/*
	
	System.out.println("************Reservation Request****************");
	
	ReservationRequest reservationRequest 	= new ReservationRequest();
	
	//reservationRequest.getProgram().g
	
	//Contact[] contact= new Contact[2];
	//contact[0].setEmailAddress("");
	//contact[0].setTitle("Mr");
	//contact[0].setFirstName("Ruchira");
	//contact[0].setLastName("Kariyawasam");
	
	//reservationRequest.setReservationContact(contact);		
	
	ProgramOccupant[] programOccupants = new ProgramOccupant[2];			
	
	ProgramOccupant programOccupant1 = new ProgramOccupant();	
	ProgramOccupant programOccupant2 = new ProgramOccupant();	
	//ProgramOccupant programOccupant3 = new ProgramOccupant();	
				
	programOccupant1.setOccupantFirstName("Ruchira");
	programOccupant1.setOccupantLastName("Test");
	programOccupant1.setOccupantTittle("Mr");
	programOccupant1.setOccupantType("ADULT");
	
	programOccupant2.setOccupantFirstName("Big");
	programOccupant2.setOccupantLastName("Cena");
	programOccupant2.setOccupantTittle("Mst");
	programOccupant2.setOccupantType("CHILD");
	programOccupant2.setChildAges("15");	
	
//	programOccupant3.setOccupantFirstName("Little");
//	programOccupant3.setOccupantLastName("Cena");
//	programOccupant3.setOccupantTittle("Mst");
//	programOccupant3.setOccupantType("INFANT");
//	programOccupant3.setChildAges("10");			
			
	programOccupants[0] = programOccupant1;
	programOccupants[1] = programOccupant2;	
	//programOccupants[2] = programOccupant3;	
	
//	
//	ProgramOccupant programOccupant2 = new ProgramOccupant();
//	programOccupant2.setOccupantFirstName("Test1CHILD");
//	programOccupant2.setOccupantLastName("Test1CHILD");
//	programOccupant2.setOccupantTittle("Mr");
//	programOccupant2.setOccupantType("CHILD");
//	programOccupants[1] = programOccupant2;
//	
//	ProgramOccupant programOccupant3 = new ProgramOccupant();
//	programOccupant3.setOccupantFirstName("Testfgg1");
//	programOccupant3.setOccupantLastName("Tefgfgst1");
//	programOccupant3.setOccupantTittle("Mrs");
//	programOccupant3.setOccupantType("CHILD");
//	programOccupants[2] = programOccupant3;
//	
//	ProgramOccupant programOccupant4 = new ProgramOccupant();
//	programOccupant4.setOccupantFirstName("Test1");
//	programOccupant4.setOccupantLastName("Test1");
//	programOccupant4.setOccupantTittle("Mst");
//	programOccupant4.setOccupantType("CHILD");
//	programOccupant4.setChildAges(childAges);
//	programOccupants[3] = programOccupant4;
//	
//	ProgramOccupant programOccupant5 = new ProgramOccupant();
//	programOccupant5.setOccupantFirstName("Test1");
//	programOccupant5.setOccupantLastName("Test1");
//	programOccupant5.setOccupantTittle("Mst");
//	programOccupant5.setOccupantType("CHILD");
//	programOccupant5.setChildAges(childAges);
//	programOccupants[4] = programOccupant5;
	
	((Activity)this.programResProgram.getActivity().get(0)).setProgramOccupants(programOccupants);

	this.programResProgram.setVendorProgramData("leadPaxID"				, "1"				);
	this.programResProgram.setVendorProgramData("pickUpTime"			, time1				);
	this.programResProgram.setVendorProgramData("dropOffTime"			, time2				);
	this.programResProgram.setVendorProgramData("pickUpFlightNumber"	, flightNumber		);
	this.programResProgram.setVendorProgramData("dropOffFlightNumber"	, flightNumber		);
	this.programResProgram.setVendorProgramData("address1"				, address1			);
	this.programResProgram.setVendorProgramData("address2"				, address2			);
	this.programResProgram.setVendorProgramData("postCode"				, postCode			);
	this.programResProgram.setVendorProgramData("shipName"				, shipName			);
	this.programResProgram.setVendorProgramData("trainName"				, trainName			);
	this.programResProgram.setVendorProgramData("shipCompany"			, shipCompany		);
	
	if(pickUpType.equals("A")){
		this.programResProgram.setVendorProgramData("arrivingFrom"			, airportcode	);
	}else if(pickUpType.equals("H")){
		this.programResProgram.setVendorProgramData("pickUpPointCode"		, hotelCode		); // Hotel
	}else if(pickUpType.equals("S")){
		this.programResProgram.setVendorProgramData("pickUpPointCode"		, stationCode	); // Hotel
		this.programResProgram.setVendorProgramData("arrivingFrom"			, adCityCode1	);
	}else if(pickUpType.equals("P")){
		this.programResProgram.setVendorProgramData("arrivingFrom"			, adCityCode1	);
	}
	
	if(dropOffType.equals("A")){
		this.programResProgram.setVendorProgramData("departingTo"			, airportcode	);
	}else if(dropOffType.equals("H")){
		this.programResProgram.setVendorProgramData("dropOffPointCode"		, hotelCode		); // Hotel
	}else if(dropOffType.equals("S")){
		this.programResProgram.setVendorProgramData("dropOffPointCode"		, stationCode	); // Hotel
		this.programResProgram.setVendorProgramData("departingTo"			, adCityCode1	);
	}else if(dropOffType.equals("P")){
		this.programResProgram.setVendorProgramData("departingTo"			, adCityCode1	);
	}


	CreditCard ccCard= new CreditCard();
	ccCard.setExpieryDate(new Date());
	ccCard.setCardNo("6111111111111116");
	ccCard.setCvsNo("123");
	ccCard.setCardType("VI");
	
	
	Contact  cn = new Contact();
	cn.setTitle("Mr");
	cn.setFirstName("Ruchira");
	cn.setLastName("Kariyawasam");
	cn.setTelephoneNo("1212121212");
	cn.setEmailAddress("ruchira@rezgateway.com");
	cn.setCard(ccCard);
	Contact[] contact1  = {cn};
	
	
	reservationRequest.setReservationContact(contact1);
	
	
	
	
	reservationRequest.setProgram(this.programResProgram);
    reservationRequest.setVendor(vendorId);
    reservationRequest.setPortal(portal);
    reservationRequest.setCheckInDate(new Date(checkInDate));
    reservationRequest.setCheckOutDate(new Date(checkOutDate));
//    this.selectedProgramObj.setVendorProgramData("adultCount","1");
//    this.selectedProgramObj.setVendorProgramData("childCount","0");
//    this.selectedProgramObj.setVendorProgramData("infantCount","0");
//    this.selectedProgramObj.setVendorProgramData("ageList","30,");
////    this.selectedProgramObj.setVendorProgramData("ageList","30,30,2");
    
    
//	    this.vendorProperties = this.selectedProgramObj.getVendorProgramData();
//	    reservationRequest.setVendorSpecificProperties(this.vendorProperties);
    reservationRequest.setRequestType(3);
    
	ServiceData serviceData = new ServiceData();
	serviceData.setServiceName("RIDEActivityService");
	serviceData.setActionMethod("");
	
	serviceData.setInputData(reservationRequest);
	
	RIDEServer server = new RIDEServer();
	server.doService(serviceData);
	
	ReservationResponse reservationResponse= (ReservationResponse) serviceData.getOutputData(0);
	errors = (ArrayList) reservationResponse.getErrors();
	
	if (errors.size() > 0) {
		for (int i = 0; i < errors.size(); i++) {
			RIDEErrors error = (RIDEErrors)errors.get(i);
			System.out.println("ErrorCode :: " + error.getErrorCode()+ "	Error :: " + error.getError());
		}
	} else {			
		for (int i = 0; i < reservationResponse.getConfirmationNo().size(); i++) {
			System.out.println("Confimation No : " + (i+1) + " >> " + reservationResponse.getConfirmationNo().get(i));
		}
		for (int i = 0; i < reservationResponse.getReservatioNo().size(); i++) {
			System.out.println("Reservation No : " + (i+1) + " >> " + reservationResponse.getReservatioNo().get(i));
		}
		itenaryIds = new String[reservationResponse.getItenaryId().size()];
		for (int i = 0; i < reservationResponse.getItenaryId().size(); i++) {
			System.out.println("Itenary Id : " + (i+1) + " >> " + reservationResponse.getItenaryId().get(i));
			itenaryIds[i] = (String)reservationResponse.getItenaryId().get(i);
		}
		
		System.out.println("----Vendor Props-------");
		for (Iterator iter = reservationResponse.getVendorResponseProps().keySet().iterator(); iter.hasNext();) {
			String key = (String) iter.next();
			System.out.println(key + " ::: " + reservationResponse.getVendorResponseProps().getProperty(key));
		}
		
		this.vendorProperties = reservationResponse.getVendorResponseProps();
		
	}

	*/
}

public void TransferCancelationCall() throws Exception {
	

	System.out.println("************Reservation Cancellation Request****************");
	
	
	String[] itenaryIds = {""};
	
	CancellationRequest cancellationRequest = new CancellationRequest();
	cancellationRequest.setPortal(portal);
	cancellationRequest.setVendor(vendorid);
	cancellationRequest.setIternaryNo(itenaryIds);
	cancellationRequest.setVendorSpecificProperties(this.vendorProps);
	cancellationRequest.setRequestType(4);
	
	Properties prop = new Properties();
	prop.put("customercountry","GB");
	cancellationRequest.setVendorSpecificProperties(prop);
	
	ServiceData cancellationServiceData = new ServiceData();
	cancellationServiceData.setActionMethod("");
	cancellationServiceData.setServiceName("RIDEActivityService");
	cancellationServiceData.setInputData(cancellationRequest);

	RIDEServer server = new RIDEServer();
	server.doService(cancellationServiceData);
	
	CancellationResponse cancellationResponse = (CancellationResponse) cancellationServiceData.getOutputData(0);
	
	errors = (ArrayList) cancellationResponse.getErrors();
	
	if (errors.size() > 0) {
		for (int i = 0; i < errors.size(); i++) {
			RIDEErrors error = (RIDEErrors)errors.get(i);
			System.out.println("ErrorCode :: " + error.getErrorCode()+ "	Error :: " + error.getError());
		}
	}else{
		System.out.println("Itenary Ids : >> " + cancellationResponse.getItenaryId());
		for (int i = 0; i < cancellationResponse.getCancellationId().length; i++) {
			System.out.println("Cancellation Id : " + (i+1) + " >> " + cancellationResponse.getCancellationId()[i]);
		}
		for (int i = 0; i < cancellationResponse.getCancellationNo().length; i++) {
			System.out.println("Cancellation No : " + (i+1) + " >> " + cancellationResponse.getCancellationNo()[i]);
		}
		
		System.out.println("----Vendor Props-------");
		for (Iterator iter = cancellationResponse.getVendorResponseProps().keySet().iterator(); iter.hasNext();) {
			String key = (String) iter.next();
			System.out.println(key + " ::: " + cancellationResponse.getVendorResponseProps().getProperty(key));
		}
	}


}


public void AirSearchCall() throws Exception {
	
	logger.info("--------------RIDE Air SEARCH Call-------------");
	
	portal         = schema;
	
	//name = "Amadeus";
	//name = "GALILEO";
	name = "IMPERIAL";
	
	Properties markups = new Properties();	
	markups.setProperty(vendorid,"5");
	
	ArrayList vendors = new ArrayList();
	vendors.add(vendorid);

	rezg.ride.air.server.availability.AvailabilityRequest availabilityRequest = new rezg.ride.air.server.availability.AvailabilityRequest();
	availabilityRequest.setId(id);
	availabilityRequest.setName(name);
	//availabilityRequest.setNameList(nameList);			
/*	availabilityRequest.setPickUpDateTime(pickUpDateTime);
	availabilityRequest.setReturnDateTime(returnDateTime);
	availabilityRequest.setPickUpLocationCode(pickUpLocationCode);
	availabilityRequest.setReturnLocationCode(returnLocationCode);		*/
	availabilityRequest.setPortal(portal);
	availabilityRequest.setMarkups(markups);
	availabilityRequest.setVendorList(vendors);
	//availabilityRequest.setCustomerCountryCode("UK");
	
	Properties prop = new Properties();
	prop.put("CodeContext","GALILEO");
	prop.put("driverAge","30");
	availabilityRequest.setVendorSpecificProperties(prop);


	ServiceData serviceData = new ServiceData();
	serviceData.setServiceName("RIDEService");
	serviceData.setActionMethod(""); 
	serviceData.setInputData(availabilityRequest);
	rezg.ride.air.server.services.RIDEServer server = new rezg.ride.air.server.services.RIDEServer();
	server.doService(serviceData);
	rezg.ride.air.server.availability.AvailabilityResponse availabilityResponse = (rezg.ride.air.server.availability.AvailabilityResponse) serviceData.getOutputData(0);

	List<RIDEErrors> errorList = availabilityResponse.getErrors();
	logger.info("Error Size >>"+errorList.size());

	if(errorList.size() > 0){
	for(int d = 0; d < errorList.size(); d++){
		RIDEErrors errorObj = (RIDEErrors) errorList.get(d);
		logger.info("--------------------Errors--------------------------");
		logger.info("Error Location->" +errorObj.getErrorLocation());
		logger.info("Error Type->" +errorObj.getErrorType());
		logger.info("Error Code->" +errorObj.getErrorCode());
		logger.info("Error->" +errorObj.getError());
	}
	
}
	
	VendorResponse Obj = new VendorResponse();
	
	
	for (int xx = 0; xx < availabilityResponse.getResponseList().size(); xx++) {
		
		Obj = (VendorResponse) availabilityResponse.getResponseList().get(xx); 
		if(xx == 0){
			vendorProps = Obj.getVendorSpecificData();
		}
		logger.info("PickUpDateTime - "+Obj.getPickUpDateTime());
		logger.info("ReturnDateTime - "+Obj.getReturnDateTime());
		logger.info("PickUpLocation - "+Obj.getPickUpLocation());
		logger.info("PickUpLocationCode - "+Obj.getPickUpLocationCode());
		logger.info("ReturnLocationCode - "+Obj.getReturnLocationCode());
		logger.info("ReturnLocation - "+Obj.getReturnLocation());
		
		ArrayList vehicleList = Obj.getVehicle();
		logger.info(vehicleList.size());
		
		for(int i = 0; i < vehicleList.size(); i++){						
			logger.info("-----------------------------------");
			logger.info("VendorCodeVehicle - " +(i+1));	

			Vehicle vehicleObj = (Vehicle) vehicleList.get(i);
			
			resVehicleObj = vehicleObj;
			
			logger.info("AirCondition - "+vehicleObj.getAirCondition());
			logger.info("TransmissionType - "+vehicleObj.getTransmissionType());
			logger.info("VehicleCategory - "+vehicleObj.getVehicleCategory());
			logger.info("VehicleClass - "+vehicleObj.getVehicleClass());
			//logger.info("VendorAirType - "+vehicleObj.getVendorAirType());
			
			Vendor venObj = vehicleObj.getVendorObj();
			logger.info("Vendor Code - "+venObj.getVendorCode());
			logger.info("Vendor Name - "+venObj.getVendor());
			logger.info("Vendor Context - "+venObj.getVendorContext());
			
			Rental rentalObj = vehicleObj.getRentalObj();
			logger.info("DistUnitName - "+rentalObj.getDistUnitName());
			logger.info("DistUnlimited - "+rentalObj.getDistUnlimited());
			logger.info("PeriodUnitName - "+rentalObj.getPeriodUnitName());
			logger.info("RateQualifier - "+rentalObj.getRateQualifier());
			logger.info("vendorCurrencyInfo- "+vehicleObj.getRentalObj().getVendorSpecificData().getProperty("vendorCurrencyInfo"));
			logger.info("TotalAmount - "+rentalObj.getTotalAmount());									
			
			logger.info("vehicleObj.getImageUrl() - "+vehicleObj.getImageUrl());
			
			logger.info("vehicleObj.getVehicleCategory() - "+vehicleObj.getVehicleCategory());
			
			
	
			
			
			logger.info("getRefundableBranchDepositAmount() XML- "+rentalObj.getRefundableBranchDepositAmount());
			
			
			ArrayList chargeList = rentalObj.getChargeList();
			logger.info("Chrage Length --> "+chargeList.size());
			for(int v = 0; v < chargeList.size(); v++){
				logger.info("----------------- Charge ------------------");
				Charge chargeObj = (Charge) chargeList.get(v);
				logger.info("ChargeAmount - "+chargeObj.getChargeAmount());
				logger.info("CurrencyCode - "+chargeObj.getCurrencyCode());
				logger.info("GuaranteedInd - "+chargeObj.getGuaranteedInd());
				logger.info("Purpose - "+chargeObj.getPurpose());
				logger.info("TaxInclusive - "+chargeObj.getTaxInclusive());
				logger.info("Description - "+chargeObj.getDescription());
				logger.info("IncludedInRate - "+chargeObj.getIncludedInRate());
			}
			
			/*Charge chargeObj = rentalObj.getCharge();
			 logger.info("ChargeAmount - "+chargeObj.getChargeAmount());
			 logger.info("CurrencyCode - "+chargeObj.getCurrencyCode());
			 logger.info("GuaranteedInd - "+chargeObj.getGuaranteedInd());
			 logger.info("Purpose - "+chargeObj.getPurpose());
			 logger.info("TaxInclusive - "+chargeObj.getTaxInclusive()); */
		}
	}


}


public void ExchangeRatesUpdate() throws Exception {
    
	try{

		
		RIDECurrencyServer rideCurrencyServer = new RIDECurrencyServer();
		
		ride.services.connectorservices.ServiceData rideServiceData = new ride.services.connectorservices.ServiceData();
		
		rideServiceData.setServiceName("RIDECurrencyService");
		rideServiceData.setSchemaName("travelstay");
		rideServiceData.setModuleName("hotels");
		rideServiceData.setInputData("travelstay");	
		
		rideServiceData.setActionMethod("updateExchangeRates");
		//rideServiceData.setActionMethod("setExchangeRates");
		rideServiceData.setActionMethod("getExchangeRates");
			
		List<ExchangeRateType> 		exchangeRateTypes		= new ArrayList<ExchangeRateType>();
		
		ExchangeRateType			exchangeRateType		= new ExchangeRateType();
		exchangeRateType.setBasecurrency(CurrencyCodeType.USD);
		exchangeRateType.setQuotecurrency(CurrencyCodeType.GBP);
		exchangeRateType.setConversiondate(new Date());
		exchangeRateType.setUpdateddate(new Date());
		exchangeRateType.setPortal("travelstay");
		exchangeRateType.setSellingrate(new Double(1.57));
		exchangeRateType.setBuyingrate(new Double(1.58));
		exchangeRateType.setSource("Internal");
		exchangeRateTypes.add(exchangeRateType);
		
		//rideServiceData.setInputData(exchangeRateTypes); // setExchangeRates from this portal
		
		rideCurrencyServer.doService(rideServiceData);
		
		logger.info("--Going to execute in Portal--");
		
		//rideServiceData = rezService.executeService(rideServiceData);
		
		logger.info("--ExecuteService Over in Portal--");
		
		Object obj = rideServiceData.getOutputData(0);

		logger.info("out obj--->"+obj);

		List<ExchangeRateType> arr = (List<ExchangeRateType>)obj;
		
		for (ExchangeRateType exchangeRateType2 : arr) {
			logger.info("----------------------------------------------------------------");
			logger.info(exchangeRateType2);
			logger.info(exchangeRateType2.getUpdateddate());
			logger.info(exchangeRateType2.getSellingrateinverse());
			logger.info(exchangeRateType2.getBuyingrateinverse());
		}

        }catch (Exception e) {
			// TODO: handle exception
        	e.printStackTrace();
		}

}

	public void HotelContentDataService() {
		
		try{
	

		    HotelContentRequest 	hotelContentRequest 		= new HotelContentRequest();
		    
		    hotelContentRequest.setRequestHotelID(testHotelCode);
		    hotelContentRequest.setVendorId(vendorid);
		   
//		    Properties vendorSpecificProperties = new Properties();
//		    vendorSpecificProperties.setProperty("transactionIdentifier", "20012112");
//		    hotelContentRequest.setVendorSpecificProperties(vendorSpecificProperties);
		   
		    ride.services.connectorservices.ServiceData serviceData = new ride.services.connectorservices.ServiceData();
		    serviceData.setActionMethod("GetHotelContentData");
		    serviceData.setSchemaName(schema);          // eg : rezbase_v3
		    serviceData.setServiceName("RIDEService");
		    serviceData.setModuleName("data");
		    serviceData.setInputData(hotelContentRequest);
		   
		    RIDEServer server = new RIDEServer();
		    server.doService(serviceData);
		   
		    HotelContentDTO HotelContentResponse = (HotelContentDTO) serviceData.getOutputData(0);
			
		}catch(Exception e){
	
		}
		
	}

	public void HotelDataService() {
		
		
		try {
			
			HotelDataRequest								hotelDataRequest	= new HotelDataRequest();
			
			hotelDataRequest.setCityCode(1112);
			//hotelDataRequest.setSuburbcode(3666);
			hotelDataRequest.setPortal("rezbase_v3");
			ArrayList<String>     							hotelDataVendors	= new ArrayList<String>();
			hotelDataVendors.add("rezbase_v3");			
			hotelDataVendors.add("hotelbeds_v2");
			hotelDataVendors.add("miki");
			hotelDataVendors.add("INV27");
			//hotelDataVendors.add("amadeus");
			
			hotelDataRequest.setVendors(hotelDataVendors);
			hotelDataRequest.setHotelDataFilterType(HotelDataFilterType.allHotels);
			hotelDataRequest.setLanguage(Language.en);
			hotelDataRequest.setAmenityRequired(true);
			hotelDataRequest.setSummaryRequired(true);
			hotelDataRequest.setGeoCodeRequired(true);
			//String[] primeVendors = {"hotelbeds_v2" , "miki"};
			//hotelDataRequest.setPrimeVendors(primeVendors);
			
			ride.services.connectorservices.ServiceData 	rideServiceData 	= new ride.services.connectorservices.ServiceData();

			rideServiceData.setServiceName("RIDEService");
			rideServiceData.setSchemaName(schema);
			rideServiceData.setModuleName("data");
			rideServiceData.setActionMethod("GetHotelData");
			rideServiceData.setInputData(hotelDataRequest); 

			RIDEServer rideServer = new RIDEServer();
			rideServer.doService(rideServiceData);
			
			logger.info("--ExecuteDataService Over--");
			
			Object obj = rideServiceData.getOutputData(0);
			
			logger.info("out obj--->"+obj);
			
			HotelDataResponse 		hotelDataResponse   = (HotelDataResponse)obj;
			List<VendorHotel> vhotels = hotelDataResponse.getHotelList().getVendorHotel();
			
			int j = 1;
			
			for (VendorHotel vendorHotel : vhotels) {
				
				int k = 1;
				
				for (Hotel hotel : vendorHotel.getHotel()) {
					logger.info( j + " " + k + " " + hotel + " suburb-->" + hotel.getSuburb());
					k++;
				}
				
				j++;
			}
			
			
		} catch (RezException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void GetNearByAttractions() {
		
		
		try {
			
			NearByAttractionsRequest	nearByAttractionsRequest	= new NearByAttractionsRequest();
			
			nearByAttractionsRequest.setCityCode(1112);
			nearByAttractionsRequest.setPortal("rezbase_v3");
			nearByAttractionsRequest.setLanguage(Language.en);

			ride.services.connectorservices.ServiceData 	rideServiceData 	= new ride.services.connectorservices.ServiceData();

			rideServiceData.setServiceName("RIDEService");
			rideServiceData.setSchemaName(schema);
			rideServiceData.setModuleName("data");
			rideServiceData.setActionMethod("GetNearByAttractionsByGroups");
			rideServiceData.setInputData(nearByAttractionsRequest); 

			RIDEServer rideServer = new RIDEServer();
			rideServer.doService(rideServiceData);
			
			logger.info("--ExecuteDataService Over--");
			
			Object obj = rideServiceData.getOutputData(0);
			
			logger.info("out obj--->"+obj);
			
//			NearByAttractionsResponse 		nearByAttractionsResponse   = (NearByAttractionsResponse)obj;
			NearByAttractionsResponse nearByAttractionsResponse = (NearByAttractionsResponse) obj;
			logger.info("main().nearByAttractionsResponse.getNearByAttractionGroups().size() : " + nearByAttractionsResponse.getNearByAttractionGroups().size());
			for(NearByAttractionGourp nearByAttractionGourp : nearByAttractionsResponse.getNearByAttractionGroups()){
				
				logger.info("====================================================================================");
				logger.info("main().nearByAttractionGourp : " + nearByAttractionGourp.getId() + " | "+ nearByAttractionGourp.getName());
				logger.info("main().nearByAttractionGourp.getNearByAttractions().size() : " + nearByAttractionGourp.getNearByAttractions().size());
				for(NearByAttraction nearByAttraction : nearByAttractionGourp.getNearByAttractions()){
					logger.info("\tmain().nearByAttraction.getNearByAttractionID() : " + nearByAttraction.getNearByAttractionID()+ " | "+nearByAttraction.getNearByAttractionName()+ " | "+ nearByAttraction.getLatitude()+ " | "+ nearByAttraction.getLongitude());
				}
				logger.info("");
			}
			
		} catch (RezException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
public void RunService() {
		
		
		try {
			
			ride.services.connectorservices.ServiceData 	rideServiceData 	= new ride.services.connectorservices.ServiceData();
			HotelDataRequest								hotelDataRequest	= new HotelDataRequest();
			
			rideServiceData.setServiceName("RIDEService");
			rideServiceData.setSchemaName(schema);
			rideServiceData.setModuleName("data");
			rideServiceData.setActionMethod("RunService");
			rideServiceData.setInputData(hotelDataRequest);
			rideServiceData.setInputData("ride.scheduled.EveryDayMidnight.MemCachedHotelAndCityData");  // any class file to run on the server
			//rideServiceData.setInputData("ride.scheduled.EveryDayMidnight.ExchangeRates");
			
			RIDEServer rideServer = new RIDEServer();
			rideServer.doService(rideServiceData);
			
			logger.info("--ExecuteDataService Over--");
			
			Object obj = rideServiceData.getOutputData(0);
			
			logger.info("out obj--->"+obj);
			
			
		} catch (RezException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	

	public void SystemTrace() throws Exception {
		
		try{
			
			//RezService rezService = ride.services.connectorservices.ServiceObjectFactory.getService("92.52.108.48","1099","ride");
			
			
			RIDETraceServer rideTraceServer = new RIDETraceServer();
			
			ride.services.connectorservices.ServiceData rideServiceData = new ride.services.connectorservices.ServiceData();
			
			rideServiceData.setServiceName("RIDETraceService");
			rideServiceData.setSchemaName("travelwinds");			
			rideServiceData.setModuleName("hotels");			
			
			rideServiceData.setActionMethod("getRideTrace");
			//rideServiceData.setInputData("reqtime > ( now() - interval '5 days')");
			rideServiceData.setInputData("reqtime BETWEEN '2013-01-01' and '2013-01-10'");
			
			//rideServiceData.setActionMethod("updateRideTrace");
			//rideServiceData.setInputData("2477855491");
			
			//rideServiceData = rezService.executeService(rideServiceData);
			
			rideTraceServer.doService(rideServiceData);

			logger.info("out-->"+rideServiceData.getOutputData(0));
			
			if(rideServiceData.getOutputData(0) instanceof List<?>){
				
				List<SystemTrace> systemTraces = (List<SystemTrace>) rideServiceData.getOutputData(0);
				
				for (SystemTrace systemTrace : systemTraces) {
					
					
					logger.info(systemTrace);
					
				}
				
			}
			
			
	    }catch (Exception e) {
			// TODO: handle exception
	    	e.printStackTrace();
		}
		
	}



	static class testType{
	
	private testType(String type){
		test = type;
	}
	
	public String getTest() {
		return test;
	}
	
	private	String test 		= null;
		
	private static testType hotelSearchCall 			= new testType("hotelSearchCall");
	private static testType hotelRoomSearchCall 		= new testType("hotelRoomSearchCall");
	private static testType hotelPreReservationCall 	= new testType("hotelPreReservationCall");
	private static testType hotelReservationCall 		= new testType("hotelReservationCall");
	private static testType hotelCancelationCall 		= new testType("hotelCancelationCall");
	private static testType hotelContent 				= new testType("hotelContent");
	
	private static testType carSearchCall 				= new testType("carSearchCall");
	private static testType carContentCall 				= new testType("carContentCall");
	private static testType carPreReservationCall 		= new testType("carPreReservationCall");
	private static testType carReservationCall 			= new testType("carReservationCall");
	private static testType carCancelationCall 			= new testType("carCancelationCall");
	
	private static testType transferAvailabilityCall 				= new testType("transferAvailabilityCall");
	private static testType transferProgramAvailabilityCall 				= new testType("transferProgramAvailabilityCall");
	private static testType transferProgramContentCall 		= new testType("transferProgramContentCall");
	private static testType transferReservationCall 			= new testType("transferReservationCall");
	private static testType transferCancelationCall 			= new testType("transferCancelationCall");

	private static testType airSearchCall 				= new testType("airSearchCall");

	
	}
	
	
}
